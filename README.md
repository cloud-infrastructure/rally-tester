rally-tester
============

![Build status](https://gitlab.cern.ch/cloud-infrastructure/rally-tester/badges/master/build.svg)
![coverage](https://gitlab.cern.ch/cloud-infrastructure/rally-tester/badges/master/coverage.svg?job=coverage)

Tool to trigger rally jobs, grab the results and sent them to GNI
