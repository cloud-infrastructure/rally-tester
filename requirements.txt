setuptools
pbr!=2.1.0,>=2.0.0
oslo_config
influxdb
yaql==2.0.0