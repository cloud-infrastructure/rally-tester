import unittest

from influxdb.exceptions import InfluxDBClientError
from parameterized import parameterized
from rallytester.reporter.reporter import Reporter
from rallytester.tests import fixtures
from unittest import mock


class TestReporter(unittest.TestCase):

    maxDiff = None

    @parameterized.expand([
        (
            'keystone',
            'authenticate',
            {},
            fixtures.REPORT_AUTHENTICATE,
            fixtures.MEASURES_AUTHENTICATE
        ),
        (
            'nova',
            'boot-linux',
            {
                'avz_zone': 'cern-geneva-a',
                'flavor_name': 'rally.tiny',
                'image_name': 'Rally CirrOS 0.6.0-x86_64',
                'region_name': 'next'
            },
            fixtures.REPORT_BOOT_SERVER,
            fixtures.MEASURES_BOOT_SERVER
        ),
        (
            'nova',
            'boot_server',
            {},
            fixtures.REPORT_FAIL_BOOT_SERVER,
            fixtures.MEASURES_FAIL_BOOT_SERVER
        ),
        (
            'nova',
            'boot_snapshot',
            {},
            fixtures.REPORT_BOOT_SNAPSHOT,
            fixtures.MEASURES_BOOT_SNAPSHOT
        ),
        (
            'nova',
            'boot_volume',
            {},
            fixtures.REPORT_BOOT_VOLUME,
            fixtures.MEASURES_BOOT_VOLUME
        ),
        (
            'glance',
            'snapshot_server',
            {},
            fixtures.REPORT_SNAPSHOT_SERVER,
            fixtures.MEASURES_SNAPSHOT_SERVER
        ),
        (
            'nova',
            'boot_server',
            {},
            fixtures.REPORT_FAILURE_BOOT,
            fixtures.MEASURES_FAILURE_BOOT
        ),
        (
            'ironic',
            'boot-baremetal',
            {},
            fixtures.REPORT_CRASHED_IRONIC,
            fixtures.MEASURES_CRASHED_IRONIC
        ),
        (
            'glance',
            'create-and-delete-image',
            {},
            fixtures.REPORT_CRASHED_GLANCE,
            fixtures.MEASURES_CRASHED_GLANCE
        ),
        (
            'octavia',
            'l7-lb-redirect',
            {},
            fixtures.REPORT_FAILURE_OCTAVIA,
            fixtures.MEASURES_FAILURE_OCTAVIA
        ),
    ])
    def test_analyze_report_parametrized(self, component, scenario,
                                         extra_tags, report, measurements):
        with mock.patch("socket.gethostname", return_value="fake"):
            self.assertListEqual(
                Reporter().analyze_report(
                    component=component,
                    scenario=scenario,
                    extra_tags=extra_tags,
                    report=report
                ),
                measurements
            )

    @mock.patch('rallytester.reporter.backends.influx.InfluxDBClient')
    def test_send_report(self, mock_client):
        mock_client.return_value.write_points.return_value = True

        with mock.patch("socket.gethostname", return_value="fake"):
            Reporter().send_report(
                task='nova/boot-linux.json',
                task_args={
                    'avz_zone': 'cern-geneva-a',
                    'flavor_name': 'rally.tiny',
                    'image_name': 'Rally CirrOS 0.6.0-x86_64',
                    'region_name': 'next'
                },
                report=fixtures.REPORT_BOOT_SERVER
            )
            mock_client.return_value.write_points.assert_called_with(
                fixtures.MEASURES_BOOT_SERVER
            )

    @mock.patch('rallytester.reporter.backends.influx.InfluxDBClient')
    def test_send_measurements_success(self, mock_client):
        mock_client.return_value.write_points.return_value = True

        Reporter().report_measurements(fixtures.MEASURES_FAILURE_BOOT)

        mock_client.return_value.write_points.assert_called_with(
            fixtures.MEASURES_FAILURE_BOOT
        )

    @mock.patch('rallytester.reporter.backends.influx.InfluxDBClient')
    def test_send_measurements_failure(self, mock_client):
        mock_client.return_value.write_points.return_value = False

        Reporter().report_measurements(fixtures.MEASURES_FAILURE_BOOT)

        mock_client.return_value.write_points.assert_called_with(
            fixtures.MEASURES_FAILURE_BOOT
        )

    @mock.patch('rallytester.reporter.backends.influx.InfluxDBClient')
    def test_send_measurements_throw_exception(self, mock_client):
        mock_client.return_value.write_points.side_effect = (
            iter([InfluxDBClientError("error")]))

        Reporter().report_measurements(fixtures.MEASURES_FAILURE_BOOT)

        mock_client.return_value.write_points.assert_called_with(
            fixtures.MEASURES_FAILURE_BOOT
        )
