from rallytester import logger
from rallytester.tests import fixtures
import unittest


class TestLogger(unittest.TestCase):

    def test_logging_config(self):
        config = logger.logging_config(
            level='CRITICAL',
            file_=None,
            stdout=True,
            short_task='TASK',
            deployment='DEPLOYMENT',
            component='COMPONENT')

        self.assertEqual(
            fixtures.LOGGING_STDOUT,
            config
        )

        config = logger.logging_config(
            level='CRITICAL',
            file_='/var/log/rallytester.log',
            stdout=False,
            short_task='TASK',
            deployment='DEPLOYMENT',
            component='COMPONENT')

        self.assertEqual(
            fixtures.LOGGING_FILE,
            config
        )
