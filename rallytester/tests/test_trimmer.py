import logging
import unittest

from parameterized import parameterized
from rallytester.trimmer import MsgTrimmer

log = logging.getLogger(__name__)


class TestMsgTrimmer(unittest.TestCase):

    @parameterized.expand([
        (
            "tired_waiting_active",
            "Rally tired waiting 1440.00 seconds for Server rally-9db3-aPln:"
            "176b6fd3-8a2d-4b60-a5bd-73575c4ad161 to become ('ACTIVE') current"
            " status BUILD",
            "waiting for Server to become ('ACTIVE')"
        ), (
            "dates",
            "Request Failed: some text,"
            "'code': 500, 'created': '2021-10-19T14:00:07Z'",
            "Request Failed: some text,"
            "'code': 500, 'created': 'date'"
        ), (
            "ip6",
            '"interpreter": "/bin/sh", "script_inline": "ping'
            ' -c 5 xyz.xyz.xyz.xyz && ping -6 -c 5 2001:4860:4860::8888"} "',
            '"interpreter": "/bin/sh", "script_inline": "ping'
            ' -c 5 xyz.xyz.xyz.xyz && ping -6 -c 5 wxyz:wxyz:wxyz::wxyz"} "'
        ), (
            "ip6",
            "Timeout waiting for '2001:1458:d00:4::11'",
            "Timeout waiting for wxyz:wxyz:wxyz::wxyz"
        ), (
            "quota_exceeded_cores",
            "Quota exceeded for cores, instances: Requested 1, 1, but already"
            " used 10, 10 of 10, 10 cores, instances (HTTP 403) (Request-ID: "
            "req-23b5fe81-0bab-4f42-84e9-97e3e6847c04)",
            "Quota exceeded for cores, instances: Requested ..... cores, inst"
            "ances (HTTP 403) (Request-ID: req-xyz)"
        ), (
            "tired_waiting_deleted",
            "Rally tired waiting 300.00 seconds for Server rally-31bd-EnVg:39"
            "170e31-04b1-485f-b0fd-ecadfb0894a9 to become ('DELETED') current"
            " status ACTIVE",
            "waiting for Server to become ('DELETED')"
        ), (
            "quota_exceeded_instances",
            "Quota exceeded for instances: Requested 1, but already used 5 of"
            " 5 instances (HTTP 403) (Request-ID: req-8318ddf1-f0cf-4a28-b238"
            "-59fddfbc8326)",
            "Quota exceeded for instances: Requested 1, but already used 5 of"
            " 5 instances (HTTP 403) (Request-ID: req-xyz)"
        ), (
            "resource_error",
            "Resource <Server: rally-de6f-mhLE> has ERROR status.  Fault: {u'"
            "message': u\"Request Failed: internal server error while process"
            "ing your request.\\nNeutron server returns request_ids: ['req-78"
            "ff9064-354e-43bc-b5d1-5ba31a65a6dd']\", u'code': 500, u'created'"
            ": u'2018-04-22T09:19:12Z'}",
            "Request Failed: internal server error while processing your requ"
            "est.\\nNeutron server returns request_ids: ['req-xyz']"
        ), (
            "TASK_miss_flavour",
            "Scenario plugin 'NovaServers.boot_and_bounce_server' doesn't pass"
            " image_valid_on_flavor@openstack validation. Details: Traceback ("
            "most recent call last): (1 of 19)",
            "Task 'NovaServers.boot_and_bounce_server' fail image_valid_on_fla"
            "vor@openstack"
        ), (
            "HTTP_with_PORT",
            "Error. Failed to get the resource <Server: rally-6aba-EMCK>: Unab"
            "le to connect to : https://openstack.cern.ch:8774/v2.1/022d500b-b"
            "427-4c57-91c1-549ca7dda4e9/servers/5de33ca3-3f09-45f7-8ecb-3d198a"
            "35d635: ('Connection aborted.', BadStatusLine(,))",
            "Error. Failed to get the resource <Server: rally-xyz>: Unable to "
            "connect to : https://openstack.cern.ch:8774/... ('Connection abor"
            "ted.', BadStatusLine(,))"
        ), (
            "HTTP_without_PORT",
            "Failed to get the resource <Server: rally-33e8-BbKc>: Unable to e"
            "stablish connection to https://openstack.cern.ch:8774/v2.1/d6bc77"
            "f1-9721-479a-9cbb-4e4338675d8a/servers/03037754-f48e-4cb7-b3df-48"
            "70aed5099e: ('Connection aborted.', BadStatusLine(,))",
            "Failed to get the resource <Server: rally-xyz>: Unable to establi"
            "sh connection to https://openstack.cern.ch:8774/... ('Connection "
            "aborted.', BadStatusLine(,))"
        ), (
            "resource_limit",
            "2020-12-14 09:22:23.333 205319 ERROR rallytester.engine [-] "
            "[magnum k8s-cve-scan magnum_t_10_119] Task failed: Failure rate "
            "criteria 0.00% <= 0.00% <= 0.00% - Passed Unexpected error: "
            "Resource limit exceeded: You have reached the maximum clusters "
            "per project, 20 (HTTP 403) "
            "(Request-ID: req-488facf8-c0b2-4292-9d3b-368b4df9ddd7) "
            "Failure rate criteria 0.00% <= 0.00% <= 0.00% - Passed "
            "Unexpected error: Resource limit exceeded: You have reached the "
            "maximum clusters per project, 20 (HTTP 403) (Request-ID: "
            "req-xyz)",
            "Resource limit exceeded: You have reached the maximum clusters "
            "per project"
        ), (
            "migration",
            "Live Migration failed: Migration complete but "
            "instance did not change host: i66366241007557.cern.ch",
            "migration failed"
        ), (
            "dates",
            "GetResourceErrorStatus\nResource <Server: rally-xyz> has ERROR st"
            "atus.\n Fault: {'message': 'internal error: process exited while "
            "connecting to monitor: ioctl(KVM_CREATE_VM) failed: 12 Cannot all"
            "ocate memory\\n2022-02-09T10:15:46.166801Z qemu-kvm: failed to in"
            "itialize KVM: Cannot allocate memory',",
            "GetResourceErrorStatus\nResource <Server: rally-xyz> has ERROR st"
            "atus.\n Fault: {'message': 'internal error: process exited while "
            "connecting to monitor: ioctl(KVM_CREATE_VM) failed: 12 Cannot all"
            "ocate memory\\ndate qemu-kvm: failed to initialize KVM: Cannot al"
            "locate memory',"
        ), (
            "datetime",
            "TimeoutException\nwaiting for Job to become Complete\n 'completio"
            "n_time': None,\n 'conditions': None,\n 'failed': None,\n 'start_t"
            "ime': datetime.datetime(2022, 2, 11, 3, 43, 25, tzinfo=tzutc()),"
            "\n 'succeeded': None}",
            "TimeoutException\nwaiting for Job to become Complete\n 'completio"
            "n_time': None,\n 'conditions': None,\n 'failed': None,\n 'start_t"
            "ime': datetime,\n 'succeeded': None}"
        ), (
            "pods",
            "GetResourceErrorStatus\nResource xyz has Failed status.\n Fault: "
            "wait for cern-base-authz to be ready failed: error: timed out wai"
            "ting for the condition on pods/cern-base-authz-zrg5j",
            "GetResourceErrorStatus\nResource xyz has Failed status.\n Fault: "
            "wait for cern-base-authz to be ready failed: error: timed out wai"
            "ting for the condition on pods/podXYZ",
        ), (
            "l7 policy",
            "OctaviaClientException\nQuota has been met for resources: L7 Poli"
            "cy (HTTP 403) (Request-ID: req-bf605674-d602-47ae-b48e-223b16038d"
            "85)\nTraceback (most recent call last):\n  File \"/usr/lib/python"
            "3.9/site-packages/octaviaclient/api/v2/octavia.py\", line 34, in "
            "wrapper\n    response = func(*args, **kwargs)\n  File \"/usr/lib/"
            "python3.9/site-packages/octaviaclient/api/v2/octavia.py\", line 4"
            "99, in l7policy_create\n    response = self._create(url, **kwargs"
            ")\n  File \"/usr/lib/python3.9/site-packages/osc_lib/api/api.py\""
            ", line 162, in create\n    ret = self._request(method, url, sessi"
            "on=session, **params)\n  File \"/usr/lib/python3.9/site-packages/"
            "osc_lib/api/api.py\", line 139, in _request\n    return session.r"
            "equest(url, method, **kwargs)\n  File \"/usr/lib/python3.9/site-p"
            "ackages/keystoneauth1/session.py\", line 986, in request\n    rai"
            "se exceptions.from_response(resp, method, url)\nkeystoneauth1.exc"
            "eptions.http.Forbidden: Unrecognized schema in response body. (HT"
            "TP 403) (Request-ID: req-bf605674-d602-47ae-b48e-223b16038d85)"
            "\n\nThe above exception was the direct cause of the following exc"
            "eption:\n\nTraceback (most recent call last):\n  File \"/usr/lib/"
            "python3.9/site-packages/rally/task/runner.py\", line 69, in _run_"
            "scenario_once\n    getattr(scenario_inst, method_name)(**scenario"
            "_kwargs)\n  File \"/usr/lib/python3.9/site-packages/rally_opensta"
            "ck/task/scenarios/octavia/l7policy.py\", line 83, in run\n    sel"
            "f._populate_lb_l7(loadbalancers, listener_args,\n  File \"/usr/li"
            "b/python3.9/site-packages/rally_openstack/task/scenarios/octavia/"
            "l7policy.py\", line 59, in _populate_lb_l7\n    l7policy = self.o"
            "ctavia.l7policy_create(json={\n  File \"/usr/lib/python3.9/site-p"
            "ackages/rally/task/service.py\", line 114, in wrapper\n    return"
            " func(instance, *args, **kwargs)\n  File \"/usr/lib/python3.9/sit"
            "e-packages/rally/task/atomic.py\", line 91, in func_atomic_action"
            "s\n    f = func(self, *args, **kwargs)\n  File \"/usr/lib/python3"
            ".9/site-packages/rally_openstack/common/services/loadbalancer/oct"
            "avia.py\", line 391, in l7policy_create\n    return self._clients"
            ".octavia().l7policy_create(**kwargs)\n  File \"/usr/lib/python3.9"
            "/site-packages/octaviaclient/api/v2/octavia.py\", line 57, in wra"
            "pper\n    raise OctaviaClientException(\noctaviaclient.api.except"
            "ions.OctaviaClientException: Quota has been met for resources: L7"
            " Policy (HTTP 403) (Request-ID: req-bf605674-d602-47ae-b48e-223b1"
            "6038d85)\n",
            "OctaviaClientException\nQuota has been met for resources: L7 Poli"
            "cy (HTTP 403) (Request-ID: req-xyz)"
        )
    ])
    def test_regex(self, testname, message, result):
        trimmer = MsgTrimmer()
        self.assertIsNotNone(trimmer.nestFunctions())

        log.debug(testname)
        log.debug('Input: %s', message)
        log.debug('Expected output: %s', result)
        log.debug('Trimmer  output: %s', trimmer.trim(message))
        self.assertEqual(result, trimmer.trim(message))
