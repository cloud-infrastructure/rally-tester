import io
import logging
import unittest

from rallytester import engine
from rallytester.tests import fixtures
from unittest import mock

log = logging.getLogger(__name__)
func = 'builtins.open'


class TestRallyEngine(unittest.TestCase):

    maxDiff = None

    def test_msg_enrich(self):
        self.assertEqual(
            'empty <sep-rally-sep> empty',
            engine.RallyEngine().msg_enrich('empty')
        )

    @mock.patch('rallytester.engine.MsgTrimmer')
    def test_msg_enrich_with_trimmer(self, mock_trimmer):
        mock_trimmer.return_value.trim.return_value = 'trimmed'

        self.assertEqual(
            'empty <sep-rally-sep> trimmed',
            engine.RallyEngine().msg_enrich(
                msg='empty'
            )
        )

    def test_execute_task_not_exists(self):
        self.assertEqual(
            (
                {
                    'task': '/opt/rally-scenarios/component/fake_task.json',
                    'succeed': 0
                },
                'Task /opt/rally-scenarios/component/fake_task.json not found.'
                ' Check you have put the correct path.'
            ),
            engine.RallyEngine().execute_task(
                deployment='fake_deployment',
                task='/opt/rally-scenarios/component/fake_task.json',
                task_args={}
            )
        )

    @mock.patch('rallytester.engine.Reporter')
    @mock.patch('rallytester.engine.subprocess')
    @mock.patch(func, return_value=io.StringIO(fixtures.TASK))
    @mock.patch('os.path.exists', return_value=True)
    def test_execute_task_exists_fail(self,
                                      mock_os,
                                      mock_open,
                                      mock_sub,
                                      mock_reporter):
        mock_pr = mock_sub.Popen.return_value
        mock_pr.communicate.return_value = (b'', None)
        mock_pr.returncode.return_value = 1

        self.assertEqual(
            (
                {
                    'task': '/opt/rally-scenarios/component/fake_task.json',
                    'succeed': 0
                },
                'Error executing rally task.'
                ' Check the deployment is correctly configured.\n'
                'Complete output of `rally task start`: \n'
                '\n'
                'STDERR: \n'
                'None\n'
                'Deployment\n'
                'fake_deployment\n'
                'Task descriptor:\n'
                '\n'
                '{\n'
                '    "NovaServers.list_servers": [\n'
                '        {\n'
                '            "args": {\n'
                '                "detailed": false\n'
                '            },\n'
                '            "runner": {\n'
                '                "type": "constant",\n'
                '                "times": 1,\n'
                '                "concurrency": 1\n'
                '            },\n'
                '            "context": {}\n'
                '        }\n'
                '    ]\n'
                '}\n'
            ),
            engine.RallyEngine().execute_task(
                deployment='fake_deployment',
                task='/opt/rally-scenarios/component/fake_task.json',
                task_args={}
            )
        )

    @mock.patch('rallytester.engine.Reporter')
    @mock.patch('rallytester.engine.subprocess')
    @mock.patch(func, return_value=io.StringIO(fixtures.TASK))
    @mock.patch('os.path.exists', return_value=True)
    def test_execute_task_exists_fail_reason(self, mock_os, mock_open,
                                             mock_sub, mock_reporter):
        mock_pr = mock_sub.Popen.return_value
        mock_pr.communicate.return_value = (
            b"Reason(s):\n Flavor '{\"name\": \"rally.nano\"}' not found",
            None)
        mock_pr.returncode.return_value = 1

        self.assertEqual(
            (
                {
                    'task': '/opt/rally-scenarios/component/fake_task.json',
                    'succeed': 0
                },
                ' Flavor \'{"name": "rally.nano"}\' not found (1 of 1)'
                '\n'
                'Complete output of `rally task start`: \n'
                'Reason(s):\n Flavor \'{"name": "rally.nano"}\' not found'
                '\n'
                'STDERR: \n'
                'None\n'
                'Deployment\n'
                'fake_deployment\n'
                'Task descriptor:\n'
                '\n'
                '{\n'
                '    "NovaServers.list_servers": [\n'
                '        {\n'
                '            "args": {\n'
                '                "detailed": false\n'
                '            },\n'
                '            "runner": {\n'
                '                "type": "constant",\n'
                '                "times": 1,\n'
                '                "concurrency": 1\n'
                '            },\n'
                '            "context": {}\n'
                '        }\n'
                '    ]\n'
                '}\n'
            ),
            engine.RallyEngine().execute_task(
                deployment='fake_deployment',
                task='/opt/rally-scenarios/component/fake_task.json',
                task_args={}
            )
        )

    @mock.patch('rallytester.engine.Reporter')
    @mock.patch('rallytester.engine.subprocess.Popen')
    @mock.patch(func, return_value=io.StringIO(fixtures.TASK))
    @mock.patch('os.path.exists', return_value=True)
    def test_execute_task_exists_run1_fail2(self,
                                            mock_os,
                                            mock_open,
                                            mock_sub,
                                            mock_reporter):
        mock_pr = mock_sub.return_value
        type(mock_pr).returncode = mock.PropertyMock(return_value=1)
        mock_pr.communicate.side_effect = [
            (
                b'Using task: ac55563f-1991-45d7-b91c-c9213446d498',
                None
            ),
            (
                b'',
                None
            )
        ]

        self.assertEqual(
            (
                {
                    'task': '/opt/rally-scenarios/component/fake_task.json',
                    'succeed': 0
                },
                'Error retrieving rally task results.'
                ' Check the deployment is correctly configured.\n'
                'Complete output of `rally task results`:\n'
                'ac55563f-1991-45d7-b91c-c9213446d498\n'
                ' \n'
                '\n'
                'STDERR: \n'
                'None\n'
                'Task descriptor:\n'
                '\n'
                '{\n'
                '    "NovaServers.list_servers": [\n'
                '        {\n'
                '            "args": {\n'
                '                "detailed": false\n'
                '            },\n'
                '            "runner": {\n'
                '                "type": "constant",\n'
                '                "times": 1,\n'
                '                "concurrency": 1\n'
                '            },\n'
                '            "context": {}\n'
                '        }\n'
                '    ]\n'
                '}\n'
            ),
            engine.RallyEngine().execute_task(
                deployment='fake_deployment',
                task='/opt/rally-scenarios/component/fake_task.json',
                task_args={}
            )
        )

    @mock.patch('rallytester.engine.Reporter')
    @mock.patch('rallytester.engine.subprocess.Popen')
    @mock.patch(func, return_value=io.StringIO(fixtures.TASK))
    @mock.patch('os.path.exists', return_value=True)
    def test_execute_task_exists_json_exception(self,
                                                mock_os,
                                                mock_open,
                                                mock_sub,
                                                mock_reporter):
        mock_pr = mock_sub.return_value
        type(mock_pr).returncode = mock.PropertyMock(return_value=0)
        mock_pr.communicate.side_effect = [
            (
                b'Using task: ac55563f-1991-45d7-b91c-c9213446d498',
                None
            ),
            (
                b'',
                None
            )
        ]

        self.assertEqual(
            (
                {
                    'task': '/opt/rally-scenarios/component/fake_task.json',
                    'succeed': 0
                },
                'Rally report is not a JSON. Cannot process results.'
                '\nComplete output of `rally task result '
                'ac55563f-1991-45d7-b91c-c9213446d498`: \n'
                '\n'
                'STDERR: \n'
                'None\n'
                'Task descriptor:\n'
                '\n'
                '{\n'
                '    "NovaServers.list_servers": [\n'
                '        {\n'
                '            "args": {\n'
                '                "detailed": false\n'
                '            },\n'
                '            "runner": {\n'
                '                "type": "constant",\n'
                '                "times": 1,\n'
                '                "concurrency": 1\n'
                '            },\n'
                '            "context": {}\n'
                '        }\n'
                '    ]\n'
                '}\n'

            ),
            engine.RallyEngine().execute_task(
                deployment='fake_deployment',
                task='/opt/rally-scenarios/component/fake_task.json',
                task_args={}
            )
        )

    @mock.patch('rallytester.engine.Reporter')
    @mock.patch('rallytester.engine.subprocess.Popen')
    @mock.patch(func, return_value=io.StringIO(fixtures.TASK))
    @mock.patch('os.path.exists', return_value=True)
    def test_execute_task_exists_json_valid(self,
                                            mock_os,
                                            mock_open,
                                            mock_sub,
                                            mock_reporter):
        mock_pr = mock_sub.return_value
        type(mock_pr).returncode = mock.PropertyMock(return_value=0)
        mock_pr.communicate.side_effect = [
            (
                b'Using task: ac55563f-1991-45d7-b91c-c9213446d498',
                None
            ),
            (
                fixtures.TASK_REPORT,
                None
            )
        ]

        self.assertEqual(
            (
                {
                    'duration': 0.380141,
                    'scenarios': 'GlanceImages.list_images',
                    'succeed': 1,
                    'task': '/opt/rally-scenarios/component/fake_task.json'
                },
                None  # No errors
            ),
            engine.RallyEngine().execute_task(
                deployment='fake_deployment',
                task='/opt/rally-scenarios/component/fake_task.json',
                task_args={}
            )
        )

    @mock.patch('rallytester.engine.Reporter')
    @mock.patch('rallytester.engine.subprocess.Popen')
    @mock.patch(func, return_value=io.StringIO(fixtures.TASK))
    @mock.patch('os.path.exists', return_value=True)
    def test_execute_task_exists_json_valid_err(self,
                                                mock_os,
                                                mock_open,
                                                mock_sub,
                                                mock_reporter):
        mock_pr = mock_sub.return_value
        type(mock_pr).returncode = mock.PropertyMock(return_value=0)
        mock_pr.communicate.side_effect = [
            (
                b'Using task: ac55563f-1991-45d7-b91c-c9213446d498',
                None
            ),
            (
                fixtures.TASK_REPORT_ERROR2,
                None
            )
        ]

        self.assertEqual(
            (
                {
                    'scenarios': 'GlanceImages.list_images',
                    'succeed': 0,
                    'task': '/opt/rally-scenarios/component/fake_task.json'
                },
                '\nFake error1\n\nFake error2\nTask descriptor:\n\n'
                '{\n'
                '    "NovaServers.list_servers": [\n'
                '        {\n'
                '            "args": {\n'
                '                "detailed": false\n'
                '            },\n'
                '            "runner": {\n'
                '                "type": "constant",\n'
                '                "times": 1,\n'
                '                "concurrency": 1\n'
                '            },\n'
                '            "context": {}\n'
                '        }\n'
                '    ]\n'
                '}\n'
            ),
            engine.RallyEngine().execute_task(
                deployment='fake_deployment',
                task='/opt/rally-scenarios/component/fake_task.json',
                task_args={}
            )
        )

    @mock.patch('rallytester.engine.Reporter')
    @mock.patch('rallytester.engine.subprocess.Popen')
    @mock.patch(func, return_value=io.StringIO(fixtures.TASK))
    @mock.patch('os.path.exists', return_value=True)
    def test_execute_task_exists_json_err2(self,
                                           mock_os,
                                           mock_open,
                                           mock_sub,
                                           mock_reporter):
        mock_pr = mock_sub.return_value
        type(mock_pr).returncode = mock.PropertyMock(return_value=0)
        mock_pr.communicate.side_effect = [
            (
                b'Using task: ac55563f-1991-45d7-b91c-c9213446d498',
                None
            ),
            (
                fixtures.TASK_REPORT_ERROR1,
                None
            )
        ]

        self.assertEqual(
            (
                {
                    'scenarios': 'GlanceImages.list_images',
                    'succeed': 0,
                    'task': '/opt/rally-scenarios/component/fake_task.json'
                },
                '\n\nTask descriptor:\n\n'
                '{\n'
                '    "NovaServers.list_servers": [\n'
                '        {\n'
                '            "args": {\n'
                '                "detailed": false\n'
                '            },\n'
                '            "runner": {\n'
                '                "type": "constant",\n'
                '                "times": 1,\n'
                '                "concurrency": 1\n'
                '            },\n'
                '            "context": {}\n'
                '        }\n'
                '    ]\n'
                '}\n'
            ),
            engine.RallyEngine().execute_task(
                deployment='fake_deployment',
                task='/opt/rally-scenarios/component/fake_task.json',
                task_args={}
            )
        )

    def test_get_task_id_fake(self):
        self.assertEqual(
            '',
            engine.RallyEngine().get_pattern(
                'fake_out',
                engine.TASK_ID_PATTERN)
        )

    def test_get_task_id(self):
        self.assertEqual(
            'ac55563f-1991-45d7-b91c-c9213446d498',
            engine.RallyEngine().get_pattern(
                'Using task: ac55563f-1991-45d7-b91c-c9213446d498',
                engine.TASK_ID_PATTERN)
        )

    def test_get_reason_fake(self):
        self.assertEqual(
            '',
            engine.RallyEngine().get_pattern(
                'fake_out',
                engine.REASONS_PATTERN)
        )

    def test_get_reason(self):
        self.assertEqual(
            " Flavor '{\"name\": \"rally.nano\"}' not found",
            engine.RallyEngine().get_pattern(
                "Reason(s):\n Flavor '{\"name\": \"rally.nano\"}' not found",
                engine.REASONS_PATTERN)
        )

    def test_get_reason_multiple(self):
        self.assertEqual(
            " Flavor '{\"name\": \"rally.nano\"}' not found\n"
            " Image '{\"name\": \"my_image\"}' not found",
            engine.RallyEngine().get_pattern(
                "Reason(s):\n"
                " Flavor '{\"name\": \"rally.nano\"}' not found\n"
                " Image '{\"name\": \"my_image\"}' not found",
                engine.REASONS_PATTERN)
        )

    @mock.patch('rallytester.engine.Reporter')
    @mock.patch('rallytester.engine.subprocess.Popen')
    @mock.patch(func, return_value=io.StringIO(fixtures.TASK))
    @mock.patch('os.path.exists', return_value=True)
    def test_execute_task_crash(self,
                                mock_os,
                                mock_open,
                                mock_sub,
                                mock_reporter):
        mock_pr = mock_sub.return_value
        type(mock_pr).returncode = mock.PropertyMock(return_value=0)
        mock_pr.communicate.side_effect = [
            (
                b'Using task: 5312f2e6-f00d-4a63-9162-40c895cdaea1',
                None
            ),
            (
                fixtures.TASK_REPORT_CRASH,
                None
            )
        ]

        self.assertEqual(
            (
                {
                    'task': '/opt/rally-scenarios/component/fake_task.json',
                    'succeed': 0
                },
                'Rally task 5312f2e6-f00d-4a63-9162-40c895cdaea1 has crashed\n'
                '\nTask descriptor:\n\n'
                '{\n    "NovaServers.list_servers": '
                '[\n        {\n            "args": '
                '{\n                "detailed": false'
                '\n            },\n            "runner": '
                '{\n                "type": "constant",'
                '\n                "times": 1,\n       '
                '         "concurrency": 1\n            }'
                ',\n            "context": {}\n        }\n    ]\n}\n'
            ),
            engine.RallyEngine().execute_task(
                deployment='fake_deployment',
                task='/opt/rally-scenarios/component/fake_task.json',
                task_args={}
            )
        )
