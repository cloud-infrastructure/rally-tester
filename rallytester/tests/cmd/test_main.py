import unittest

from rallytester.cmd import main
from unittest import mock


class TestRallyTester(unittest.TestCase):

    def test_main_no_args(self):
        with self.assertRaises(SystemExit) as cm:
            main.main()
        self.assertEqual(cm.exception.code, 2)

    @mock.patch('rallytester.engine.RallyEngine.execute_task')
    def test_main_args_default(self, mock_obj):
        mock_obj.return_value = {'task': 'empty/empty_task.json',
                                 'succeed': 1}, None

        main.main([
            '--task', 'empty/empty_task.json',
            '--deployment', 'empty_deployment',
            '--debug'])

        mock_obj.assert_called_once_with(
            'empty_deployment',
            'empty/empty_task.json',
            {}
        )

    @mock.patch('rallytester.engine.RallyEngine.execute_task')
    def test_main_args_task_args_json(self, mock_obj):
        mock_obj.return_value = {'task': 'empty/empty_task.json',
                                 'succeed': 1}, None

        main.main([
            '--task', 'empty/empty_task.json',
            '--deployment', 'empty_deployment',
            '--task-args', '{"extra_tag":"value"}',
            '--debug'])

        mock_obj.assert_called_once_with(
            'empty_deployment',
            'empty/empty_task.json',
            {"extra_tag": "value"}
        )

    @mock.patch('rallytester.engine.RallyEngine.execute_task')
    def test_main_args_task_args_yaml(self, mock_obj):
        mock_obj.return_value = {'task': 'empty/empty_task.json',
                                 'succeed': 1}, None

        main.main([
            '--task', 'empty/empty_task.json',
            '--deployment', 'empty_deployment',
            '--task-args', 'extra_tag: value',
            '--debug'])

        mock_obj.assert_called_once_with(
            'empty_deployment',
            'empty/empty_task.json',
            {"extra_tag": "value"}
        )

    @mock.patch('rallytester.engine.RallyEngine.execute_task')
    def test_main_args_task_args_keyvalue(self, mock_obj):
        mock_obj.return_value = {'task': 'empty/empty_task.json',
                                 'succeed': 1}, None

        main.main([
            '--task', 'empty/empty_task.json',
            '--deployment', 'empty_deployment',
            '--task-args', 'extra_tag=value',
            '--debug'])

        mock_obj.assert_called_once_with(
            'empty_deployment',
            'empty/empty_task.json',
            {"extra_tag": "value"}
        )

    @mock.patch('rallytester.engine.RallyEngine.execute_task')
    def test_main_args_task_args_throws_error(self, mock_obj):
        mock_obj.return_value = {'task': 'empty/empty_task.json',
                                 'succeed': 1}, None

        with self.assertRaises(SystemExit) as cm:
            main.main([
                '--task', 'empty/empty_task.json',
                '--deployment', 'empty_deployment',
                '--task-args', 'ERROR',
                '--debug'])
        self.assertEqual(cm.exception.code, -1)

    @mock.patch('rallytester.engine.RallyEngine.execute_task')
    @mock.patch.object(main.notifier, 'send_alarm')
    def test_main_args_execute_error(self, mock_notifier, mock_obj):
        mock_obj.return_value = {'task': 'empty/empty_task.json',
                                 'succeed': 0}, "ERROR"

        main.main([
            '--task', 'empty/empty_task.json',
            '--deployment', 'empty_deployment',
            '--debug'])

        mock_obj.assert_called_once_with(
            'empty_deployment',
            'empty/empty_task.json',
            {}
        )

        mock_notifier.assert_called_once_with(
            'empty/empty_task.json',
            'empty_deployment',
            "ERROR",
            None
        )
