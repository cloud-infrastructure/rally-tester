import smtplib
import unittest

from rallytester import notifier
from unittest import mock


class TestNotifier(unittest.TestCase):

    @mock.patch.object(smtplib, 'SMTP')
    def test_send_alarm(self, mock_smtp):
        mock_smtp.return_value.sendmail = mock.Mock()
        mock_smtp.return_value.quit = mock.Mock()

        notifier.send_alarm(
            'fake_task',
            'fake_deployment',
            'fake_error',
            'fake@fake.domain'
        )

    def test_send_alarm_with_no_alert_to(self):
        notifier.send_alarm(
            'fake_task',
            'fake_deployment',
            'fake_error',
            None
        )
