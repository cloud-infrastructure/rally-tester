LOGGING_STDOUT = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'default': {
            'class': 'logging.StreamHandler',
            'formatter': 'standard',
            'stream': 'ext://sys.stdout'
        }
    },
    'formatters': {
        'standard': {
            'format': '%(asctime)s.%(msecs)03d %(process)d %(levelname)s %(name)s [-] [COMPONENT TASK DEPLOYMENT] %(message)s',  # noqa
            'datefmt': '%Y-%m-%d %H:%M:%S'
        }
    },
    'loggers': {
        '': {
            'handlers': ['default'],
            'level': 'CRITICAL'
        }
    }
}

LOGGING_FILE = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'default': {
            'class': 'logging.FileHandler',
            'filename': '/var/log/rallytester.log',
            'formatter': 'standard'
        }
    },
    'formatters': {
        'standard': {
            'format': '%(asctime)s.%(msecs)03d %(process)d %(levelname)s %(name)s [-] [COMPONENT TASK DEPLOYMENT] %(message)s',  # noqa
            'datefmt': '%Y-%m-%d %H:%M:%S'
        }
    },
    'loggers': {
        '': {
            'handlers': ['default'],
            'level': 'CRITICAL'
        }
    }
}

TASK = u"""
{
    "NovaServers.list_servers": [
        {
            "args": {
                "detailed": false
            },
            "runner": {
                "type": "constant",
                "times": 1,
                "concurrency": 1
            },
            "context": {}
        }
    ]
}
"""

TASK_REPORT_CRASH = b"""
{
    "info": {
        "generated_at": "2019-03-05T16:50:31",
        "rally_version": "0.0.0",
        "format_version": "1.1"
    },
    "tasks": [
        {
            "uuid": "5312f2e6-f00d-4a63-9162-40c895cdaea1",
            "title": "",
            "description": "",
            "status": "crashed",
            "tags": [],
            "created_at": "2019-03-05T15:14:54",
            "updated_at": "2019-03-05T15:14:56",
            "pass_sla": true,
            "subtasks": []
        }
    ]
}
"""

TASK_REPORT = b"""
{
    "info": {
        "generated_at": "2020-03-16T16:07:30",
        "rally_version": "2.0.0",
        "format_version": "1.2"
    },
    "tasks": [
        {
            "uuid": "8d4132ab-3530-4148-9ee3-d5bf6621aa9d",
            "title": "",
            "description": "",
            "status": "finished",
            "tags": [],
            "env_uuid": "62eb0285-247a-4b22-aecf-b1c93d757708",
            "env_name": "teststack_cell02",
            "created_at": "2020-03-15T03:17:17",
            "updated_at": "2020-03-15T03:17:20",
            "pass_sla": true,
            "subtasks": [
                {
                    "uuid": "258393e8-54b7-4e40-8344-f8f473101d12",
                    "title": "GlanceImages.list_images",
                    "description": "",
                    "status": "finished",
                    "created_at": "2020-03-15T03:17:18",
                    "updated_at": "2020-03-15T03:17:20",
                    "sla": {},
                    "workloads": [
                        {
                            "uuid": "609a426a-9ea9-4738-aee7-ea8c3bda6d48",
                            "description": "List all images.",
                            "runner": {
                                "constant": {
                                    "concurrency": 1,
                                    "times": 1
                                }
                            },
                            "hooks": [],
                            "scenario": {
                                "GlanceImages.list_images": {}
                            },
                            "min_duration": 0.1442,
                            "max_duration": 0.1442,
                            "start_time": 1584242238.24373,
                            "load_duration": 0.1442,
                            "full_duration": 0.380141,
                            "statistics": {
                                "durations": {
                                    "total": {
                                        "data": {
                                            "success": "100.0%",
                                            "min": 0.144,
                                            "max": 0.144,
                                            "median": 0.144,
                                            "95%ile": 0.144,
                                            "iteration_count": 1,
                                            "avg": 0.144,
                                            "90%ile": 0.144
                                        },
                                        "display_name": "total",
                                        "name": "total",
                                        "count_per_iteration": 1,
                                        "children": [
                                            {
                                                "data": {
                                                    "success": "100.0%",
                                                    "min": 0.144,
                                                    "max": 0.144,
                                                    "median": 0.144,
                                                    "95%ile": 0.144,
                                                    "iteration_count": 1,
                                                    "avg": 0.144,
                                                    "90%ile": 0.144
                                                },
                                                "display_name": "duration",
                                                "name": "duration",
                                                "count_per_iteration": 1,
                                                "children": []
                                            },
                                            {
                                                "data": {
                                                    "success": "100.0%",
                                                    "min": 0.0,
                                                    "max": 0.0,
                                                    "median": 0.0,
                                                    "95%ile": 0.0,
                                                    "iteration_count": 1,
                                                    "avg": 0.0,
                                                    "90%ile": 0.0
                                                },
                                                "display_name":
                                                    "idle_duration",
                                                "name": "idle_duration",
                                                "count_per_iteration": 1,
                                                "children": []
                                            }
                                        ]
                                    },
                                    "atomics": [
                                        {
                                            "data": {
                                                "success": "100.0%",
                                                "min": 0.144,
                                                "max": 0.144,
                                                "median": 0.144,
                                                "95%ile": 0.144,
                                                "iteration_count": 1,
                                                "avg": 0.144,
                                                "90%ile": 0.144
                                            },
                                            "display_name": "Novaname",
                                            "name": "Novaname",
                                            "count_per_iteration": 1,
                                            "children": []
                                        }
                                    ]
                                }
                            },
                            "data": [
                                {
                                    "timestamp": 1584242238.243732,
                                    "error": [],
                                    "duration": 0.14420008659362793,
                                    "output": {
                                        "additive": [],
                                        "complete": []
                                    },
                                    "idle_duration": 0.0,
                                    "atomic_actions": [
                                        {
                                            "finished_at":
                                                1584242238.38778,
                                            "started_at":
                                                1584242238.243803,
                                            "name": "Novaname",
                                            "children": []
                                        }
                                    ]
                                }
                            ],
                            "failed_iteration_count": 0,
                            "total_iteration_count": 1,
                            "created_at": "2020-03-15T03:17:18",
                            "updated_at": "2020-03-15T03:17:20",
                            "contexts": {},
                            "contexts_results": [
                                {
                                    "setup": {
                                        "finished_at": 1584242238.110536,
                                        "started_at": 1584242238.029151,
                                        "atomic_actions": [],
                                        "error": null
                                    },
                                    "cleanup": {
                                        "finished_at": 1584242238.408476,
                                        "started_at": 1584242238.40758,
                                        "atomic_actions": [],
                                        "error": null
                                    },
                                    "plugin_cfg": {
                                        "user_choice_method": "random"
                                    },
                                    "plugin_name": "users@openstack"
                                }
                            ],
                            "position": 0,
                            "pass_sla": true,
                            "sla_results": {
                                "sla": [
                                    {
                                        "criterion": "failure_rate",
                                        "detail":
                                            "Failure rate criteria\
                                             0.00% <= 0.00% <= 0.00%\
                                              - Passed",
                                        "success": true
                                    }
                                ]
                            },
                            "sla": {
                                "failure_rate": {
                                    "max": 0
                                }
                            }
                        }
                    ]
                }
            ]
        }
    ]
}
"""

TASK_REPORT_ERROR1 = b"""
{
    "info": {
        "generated_at": "2020-03-16T16:07:30",
        "rally_version": "2.0.0",
        "format_version": "1.2"
    },
    "tasks": [
        {
            "uuid": "8d4132ab-3530-4148-9ee3-d5bf6621aa9d",
            "title": "",
            "description": "",
            "status": "finished",
            "tags": [],
            "env_uuid": "62eb0285-247a-4b22-aecf-b1c93d757708",
            "env_name": "teststack_cell02",
            "created_at": "2020-03-15T03:17:17",
            "updated_at": "2020-03-15T03:17:20",
            "pass_sla": true,
            "subtasks": [
                {
                    "uuid": "258393e8-54b7-4e40-8344-f8f473101d12",
                    "title": "GlanceImages.list_images",
                    "description": "",
                    "status": "finished",
                    "created_at": "2020-03-15T03:17:18",
                    "updated_at": "2020-03-15T03:17:20",
                    "sla": {},
                    "workloads": [
                        {
                            "uuid": "609a426a-9ea9-4738-aee7-ea8c3bda6d48",
                            "description": "List all images.",
                            "runner": {
                                "constant": {
                                    "concurrency": 1,
                                    "times": 1
                                }
                            },
                            "hooks": [],
                            "scenario": {
                                "GlanceImages.list_images": {}
                            },
                            "min_duration": 0.1442,
                            "max_duration": 0.1442,
                            "start_time": 1584242238.24373,
                            "load_duration": 0.1442,
                            "full_duration": 0.380141,
                            "statistics": {
                                "durations": {
                                    "total": {
                                        "data": {
                                            "success": "0.0%",
                                            "min": 0.144,
                                            "max": 0.144,
                                            "median": 0.144,
                                            "95%ile": 0.144,
                                            "iteration_count": 1,
                                            "avg": 0.144,
                                            "90%ile": 0.144
                                        },
                                        "display_name": "total",
                                        "name": "total",
                                        "count_per_iteration": 1,
                                        "children": [
                                            {
                                                "data": {
                                                    "success": "0.0%",
                                                    "min": 0.144,
                                                    "max": 0.144,
                                                    "median": 0.144,
                                                    "95%ile": 0.144,
                                                    "iteration_count": 1,
                                                    "avg": 0.144,
                                                    "90%ile": 0.144
                                                },
                                                "display_name": "duration",
                                                "name": "duration",
                                                "count_per_iteration": 1,
                                                "children": []
                                            },
                                            {
                                                "data": {
                                                    "success": "0.0%",
                                                    "min": 0.0,
                                                    "max": 0.0,
                                                    "median": 0.0,
                                                    "95%ile": 0.0,
                                                    "iteration_count": 1,
                                                    "avg": 0.0,
                                                    "90%ile": 0.0
                                                },
                                                "display_name":
                                                    "idle_duration",
                                                "name": "idle_duration",
                                                "count_per_iteration": 1,
                                                "children": []
                                            }
                                        ]
                                    },
                                    "atomics": [
                                        {
                                            "data": {
                                                "success": "0.0%",
                                                "min": 0.144,
                                                "max": 0.144,
                                                "median": 0.144,
                                                "95%ile": 0.144,
                                                "iteration_count": 1,
                                                "avg": 0.144,
                                                "90%ile": 0.144
                                            },
                                            "display_name": "Novaname",
                                            "name": "Novaname",
                                            "count_per_iteration": 1,
                                            "children": []
                                        }
                                    ]
                                }
                            },
                            "data": [
                                {
                                    "timestamp": 1584242238.243732,
                                    "error": [],
                                    "duration": 0.14420008659362793,
                                    "output": {
                                        "additive": [],
                                        "complete": []
                                    },
                                    "idle_duration": 0.0,
                                    "atomic_actions": [
                                        {
                                            "finished_at":
                                                1584242238.38778,
                                            "started_at":
                                                1584242238.243803,
                                            "name": "Novaname",
                                            "children": []
                                        }
                                    ]
                                }
                            ],
                            "failed_iteration_count": 0,
                            "total_iteration_count": 1,
                            "created_at": "2020-03-15T03:17:18",
                            "updated_at": "2020-03-15T03:17:20",
                            "contexts": {},
                            "contexts_results": [
                                {
                                    "setup": {
                                        "finished_at": 1584242238.110536,
                                        "started_at": 1584242238.029151,
                                        "atomic_actions": [],
                                        "error": null
                                    },
                                    "cleanup": {
                                        "finished_at": 1584242238.408476,
                                        "started_at": 1584242238.40758,
                                        "atomic_actions": [],
                                        "error": null
                                    },
                                    "plugin_cfg": {
                                        "user_choice_method": "random"
                                    },
                                    "plugin_name": "users@openstack"
                                }
                            ],
                            "position": 0,
                            "pass_sla": true,
                            "sla_results": {
                                "sla": [
                                    {
                                        "criterion": "failure_rate",
                                        "detail": "Fake error - FAIL",
                                        "success": false
                                    }
                                ]
                            },
                            "sla": {
                                "failure_rate": {
                                    "max": 0
                                }
                            }
                        }
                    ]
                }
            ]
        }
    ]
}
"""

TASK_REPORT_ERROR2 = b"""
{
    "info": {
        "generated_at": "2020-03-16T16:07:30",
        "rally_version": "2.0.0",
        "format_version": "1.2"
    },
    "tasks": [
        {
            "uuid": "8d4132ab-3530-4148-9ee3-d5bf6621aa9d",
            "title": "",
            "description": "",
            "status": "finished",
            "tags": [],
            "env_uuid": "62eb0285-247a-4b22-aecf-b1c93d757708",
            "env_name": "teststack_cell02",
            "created_at": "2020-03-15T03:17:17",
            "updated_at": "2020-03-15T03:17:20",
            "pass_sla": true,
            "subtasks": [
                {
                    "uuid": "258393e8-54b7-4e40-8344-f8f473101d12",
                    "title": "GlanceImages.list_images",
                    "description": "",
                    "status": "finished",
                    "created_at": "2020-03-15T03:17:18",
                    "updated_at": "2020-03-15T03:17:20",
                    "sla": {},
                    "workloads": [
                        {
                            "uuid": "609a426a-9ea9-4738-aee7-ea8c3bda6d48",
                            "description": "List all images.",
                            "runner": {
                                "constant": {
                                    "concurrency": 1,
                                    "times": 1
                                }
                            },
                            "hooks": [],
                            "scenario": {
                                "GlanceImages.list_images": {}
                            },
                            "min_duration": 0.1442,
                            "max_duration": 0.1442,
                            "start_time": 1584242238.24373,
                            "load_duration": 0.1442,
                            "full_duration": 0.380141,
                            "statistics": {
                                "durations": {
                                    "total": {
                                        "data": {
                                            "success": "100.0%",
                                            "min": 0.144,
                                            "max": 0.144,
                                            "median": 0.144,
                                            "95%ile": 0.144,
                                            "iteration_count": 1,
                                            "avg": 0.144,
                                            "90%ile": 0.144
                                        },
                                        "display_name": "total",
                                        "name": "total",
                                        "count_per_iteration": 1,
                                        "children": [
                                            {
                                                "data": {
                                                    "success": "100.0%",
                                                    "min": 0.144,
                                                    "max": 0.144,
                                                    "median": 0.144,
                                                    "95%ile": 0.144,
                                                    "iteration_count": 1,
                                                    "avg": 0.144,
                                                    "90%ile": 0.144
                                                },
                                                "display_name": "duration",
                                                "name": "duration",
                                                "count_per_iteration": 1,
                                                "children": []
                                            },
                                            {
                                                "data": {
                                                    "success": "100.0%",
                                                    "min": 0.0,
                                                    "max": 0.0,
                                                    "median": 0.0,
                                                    "95%ile": 0.0,
                                                    "iteration_count": 1,
                                                    "avg": 0.0,
                                                    "90%ile": 0.0
                                                },
                                                "display_name":
                                                    "idle_duration",
                                                "name":
                                                    "idle_duration",
                                                "count_per_iteration": 1,
                                                "children": []
                                            }
                                        ]
                                    },
                                    "atomics": [
                                        {
                                            "data": {
                                                "success": "100.0%",
                                                "min": 0.144,
                                                "max": 0.144,
                                                "median": 0.144,
                                                "95%ile": 0.144,
                                                "iteration_count": 1,
                                                "avg": 0.144,
                                                "90%ile": 0.144
                                            },
                                            "display_name": "Novaname",
                                            "name": "Novaname",
                                            "count_per_iteration": 1,
                                            "children": []
                                        }
                                    ]
                                }
                            },
                            "data": [
                                {
                                    "timestamp": 1584242238.243732,
                                    "error": [
                                        "-----------",
                                        "Fake error1",
                                        "Fake error2"
                                    ],
                                    "duration": 0.14420008659362793,
                                    "output": {
                                        "additive": [],
                                        "complete": []
                                    },
                                    "idle_duration": 0.0,
                                    "atomic_actions": [
                                        {
                                            "finished_at":
                                                1584242238.38778,
                                            "started_at":
                                                1584242238.243803,
                                            "name": "Novaname",
                                            "children": []
                                        }
                                    ]
                                }
                            ],
                            "failed_iteration_count": 0,
                            "total_iteration_count": 1,
                            "created_at": "2020-03-15T03:17:18",
                            "updated_at": "2020-03-15T03:17:20",
                            "contexts": {},
                            "contexts_results": [
                                {
                                    "setup": {
                                        "finished_at": 1584242238.110536,
                                        "started_at": 1584242238.029151,
                                        "atomic_actions": [],
                                        "error": null
                                    },
                                    "cleanup": {
                                        "finished_at": 1584242238.408476,
                                        "started_at": 1584242238.40758,
                                        "atomic_actions": [],
                                        "error": null
                                    },
                                    "plugin_cfg": {
                                        "user_choice_method": "random"
                                    },
                                    "plugin_name": "users@openstack"
                                }
                            ],
                            "position": 0,
                            "pass_sla": true,
                            "sla_results": {
                                "sla": [
                                    {
                                        "criterion": "failure_rate",
                                        "detail":
                                            "Failure rate criteria\
                                             0.00% <= 0.00% <= 0.00%\
                                              - Passed",
                                        "success": true
                                    }
                                ]
                            },
                            "sla": {
                                "failure_rate": {
                                    "max": 0
                                }
                            }
                        }
                    ]
                }
            ]
        }
    ]
}
"""

REPORT_BOOT_SERVER = {
    "info": {
        "rally_version": "3.3.1~dev1",
        "generated_at": "2024-07-09T06:14:31",
        "format_version": "1.2"
    },
    "tasks": [
        {
            "uuid": "6077fdb8-e7f6-42f3-95df-7d578b0fcabb",
            "title": "",
            "description": "",
            "status": "finished",
            "tags": [],
            "env_uuid": "ae4e50d1-ccc9-4d43-9ece-6198a1226148",
            "env_name": "gva_project_050",
            "created_at": "2024-07-09T05:55:22",
            "updated_at": "2024-07-09T06:11:38",
            "pass_sla": True,
            "subtasks": [
                {
                    "uuid": "a01e8086-109c-45a6-acf7-ab42fef748ac",
                    "title": "NovaServers.boot_and_delete_server",
                    "description": "",
                    "status": "finished",
                    "created_at": "2024-07-09T05:55:23",
                    "updated_at": "2024-07-09T06:11:38",
                    "sla": {},
                    "workloads": [
                        {
                            "uuid": "445ed46d-2dd8-46e2-8c90-7d9ee7d97d06",
                            "description": "Boot and delete a server.",
                            "runner": {
                                "constant": {
                                    "times": 1,
                                    "concurrency": 1
                                }
                            },
                            "hooks": [],
                            "scenario": {
                                "NovaServers.boot_and_delete_server": {
                                    "flavor": {
                                        "name": "rally.tiny"
                                    },
                                    "image": {
                                        "name": "Rally CirrOS 0.6.0-x86_64"
                                    },
                                    "force_delete": False,
                                    "meta": {
                                        "cern-activedirectory": "false"
                                    }
                                }
                            },
                            "min_duration": 911.248,
                            "max_duration": 911.248,
                            "start_time": 1720504524.098954,
                            "load_duration": 911.248,
                            "full_duration": 972.804,
                            "statistics": {
                                "durations": {
                                    "total": {
                                        "data": {
                                            "iteration_count": 1,
                                            "min": 971.248,
                                            "median": 971.248,
                                            "90%ile": 971.248,
                                            "95%ile": 971.248,
                                            "max": 971.248,
                                            "avg": 971.248,
                                            "success": "100.0%"
                                        },
                                        "count_per_iteration": 1,
                                        "name": "total",
                                        "display_name": "total",
                                        "children": [
                                            {
                                                "data": {
                                                    "iteration_count": 1,
                                                    "min": 911.248,
                                                    "median": 911.248,
                                                    "90%ile": 911.248,
                                                    "95%ile": 911.248,
                                                    "max": 911.248,
                                                    "avg": 911.248,
                                                    "success": "100.0%"
                                                },
                                                "count_per_iteration": 1,
                                                "name": "duration",
                                                "display_name": "duration",
                                                "children": []
                                            },
                                            {
                                                "data": {
                                                    "iteration_count": 1,
                                                    "min": 60.0,
                                                    "median": 60.0,
                                                    "90%ile": 60.0,
                                                    "95%ile": 60.0,
                                                    "max": 60.0,
                                                    "avg": 60.0,
                                                    "success": "100.0%"
                                                },
                                                "count_per_iteration": 1,
                                                "name": "idle_duration",
                                                "display_name": "idle_duration",  # noqa
                                                "children": []
                                            }
                                        ]
                                    },
                                    "atomics": [
                                        {
                                            "data": {
                                                "iteration_count": 1,
                                                "min": 966.871,
                                                "median": 966.871,
                                                "90%ile": 966.871,
                                                "95%ile": 966.871,
                                                "max": 966.871,
                                                "avg": 966.871,
                                                "success": "100.0%"
                                            },
                                            "count_per_iteration": 1,
                                            "name": "nova.boot_server",
                                            "display_name": "nova.boot_server",
                                            "children": []
                                        },
                                        {
                                            "data": {
                                                "iteration_count": 1,
                                                "min": 4.377,
                                                "median": 4.377,
                                                "90%ile": 4.377,
                                                "95%ile": 4.377,
                                                "max": 4.377,
                                                "avg": 4.377,
                                                "success": "100.0%"
                                            },
                                            "count_per_iteration": 1,
                                            "name": "nova.delete_server",
                                            "display_name": "nova.delete_server",  # noqa
                                            "children": []
                                        }
                                    ]
                                }
                            },
                            "data": [
                                {
                                    "duration": 911.2479882240295,
                                    "timestamp": 1720504524.098954,
                                    "idle_duration": 60.0,
                                    "error": [],
                                    "output": {
                                        "additive": [],
                                        "complete": []
                                    },
                                    "atomic_actions": [
                                        {
                                            "name": "nova.boot_server",
                                            "children": [],
                                            "started_at": 1720504524.0992422,
                                            "finished_at": 1720505490.9698565
                                        },
                                        {
                                            "name": "nova.delete_server",
                                            "children": [],
                                            "started_at": 1720505490.9699106,
                                            "finished_at": 1720505495.346933
                                        }
                                    ]
                                }
                            ],
                            "failed_iteration_count": 0,
                            "total_iteration_count": 1,
                            "created_at": "2024-07-09T05:55:23",
                            "updated_at": "2024-07-09T06:11:38",
                            "contexts": {},
                            "contexts_results": [
                                {
                                    "plugin_name": "users@openstack",
                                    "plugin_cfg": {
                                        "user_choice_method": "random"
                                    },
                                    "setup": {
                                        "started_at": 1720504523.4162996,
                                        "finished_at": 1720504523.5246372,
                                        "atomic_actions": [],
                                        "error": None
                                    },
                                    "cleanup": {
                                        "started_at": 1720505496.2195888,
                                        "finished_at": 1720505496.2196891,
                                        "atomic_actions": [],
                                        "error": None
                                    }
                                },
                                {
                                    "plugin_name": "cleanup@openstack",
                                    "plugin_cfg": [
                                        "nova"
                                    ],
                                    "setup": {
                                        "started_at": 1720504523.5251517,
                                        "finished_at": 1720504523.525153,
                                        "atomic_actions": [],
                                        "error": None
                                    },
                                    "cleanup": {
                                        "started_at": 1720505495.3738697,
                                        "finished_at": 1720505496.2191641,
                                        "atomic_actions": [],
                                        "error": None
                                    }
                                }
                            ],
                            "position": 0,
                            "pass_sla": True,
                            "sla_results": {
                                "sla": [
                                    {
                                        "criterion": "failure_rate",
                                        "success": True,
                                        "detail": "Failure rate criteria 0.00% <= 0.00% <= 0.00% - Passed"  # noqa
                                    }
                                ]
                            },
                            "sla": {
                                "failure_rate": {
                                    "max": 0
                                }
                            }
                        }
                    ]
                }
            ]
        }
    ]
}

REPORT_AUTHENTICATE = {
    "info": {
        "rally_version": "3.3.1~dev1",
        "generated_at": "2024-07-09T18:31:18",
        "format_version": "1.2"
    },
    "tasks": [
        {
            "uuid": "7bd58c95-355f-4562-a9b2-0a7e83cc5040",
            "title": "",
            "description": "",
            "status": "finished",
            "tags": [],
            "env_uuid": "dd787fd6-3824-4007-9e04-8692f843e5bd",
            "env_name": "global",
            "created_at": "2024-07-09T16:53:05",
            "updated_at": "2024-07-09T16:53:09",
            "pass_sla": True,
            "subtasks": [
                {
                    "uuid": "7377b892-f67b-4b72-9b30-0d70cd95f035",
                    "title": "Authenticate.keystone",
                    "description": "",
                    "status": "finished",
                    "created_at": "2024-07-09T16:53:06",
                    "updated_at": "2024-07-09T16:53:09",
                    "sla": {},
                    "workloads": [
                        {
                            "uuid": "3e895431-a90f-4de5-bf2d-b5c33dba37c2",
                            "description": "Check Keystone Client.",
                            "runner": {
                                "constant": {
                                    "times": 1,
                                    "concurrency": 1
                                }
                            },
                            "hooks": [],
                            "scenario": {
                                "Authenticate.keystone": {}
                            },
                            "min_duration": 0.151682,
                            "max_duration": 0.151682,
                            "start_time": 1720543986.602751,
                            "load_duration": 0.151682,
                            "full_duration": 0.282026,
                            "statistics": {
                                "durations": {
                                    "total": {
                                        "data": {
                                            "iteration_count": 1,
                                            "min": 0.152,
                                            "median": 0.152,
                                            "90%ile": 0.152,
                                            "95%ile": 0.152,
                                            "max": 0.152,
                                            "avg": 0.152,
                                            "success": "100.0%"
                                        },
                                        "count_per_iteration": 1,
                                        "name": "total",
                                        "display_name": "total",
                                        "children": [
                                            {
                                                "data": {
                                                    "iteration_count": 1,
                                                    "min": 0.152,
                                                    "median": 0.152,
                                                    "90%ile": 0.152,
                                                    "95%ile": 0.152,
                                                    "max": 0.152,
                                                    "avg": 0.152,
                                                    "success": "100.0%"
                                                },
                                                "count_per_iteration": 1,
                                                "name": "duration",
                                                "display_name": "duration",
                                                "children": []
                                            },
                                            {
                                                "data": {
                                                    "iteration_count": 1,
                                                    "min": 0.0,
                                                    "median": 0.0,
                                                    "90%ile": 0.0,
                                                    "95%ile": 0.0,
                                                    "max": 0.0,
                                                    "avg": 0.0,
                                                    "success": "100.0%"
                                                },
                                                "count_per_iteration": 1,
                                                "name": "idle_duration",
                                                "display_name": "idle_duration",  # noqa
                                                "children": []
                                            }
                                        ]
                                    },
                                    "atomics": [
                                        {
                                            "data": {
                                                "iteration_count": 1,
                                                "min": 0.152,
                                                "median": 0.152,
                                                "90%ile": 0.152,
                                                "95%ile": 0.152,
                                                "max": 0.152,
                                                "avg": 0.152,
                                                "success": "100.0%"
                                            },
                                            "count_per_iteration": 1,
                                            "name": "authenticate.keystone",
                                            "display_name": "authenticate.keystone",  # noqa
                                            "children": []
                                        }
                                    ]
                                }
                            },
                            "data": [
                                {
                                    "duration": 0.15168190002441406,
                                    "timestamp": 1720543986.602751,
                                    "idle_duration": 0.0,
                                    "error": [],
                                    "output": {
                                        "additive": [],
                                        "complete": []
                                    },
                                    "atomic_actions": [
                                        {
                                            "name": "authenticate.keystone",
                                            "children": [],
                                            "started_at": 1720543986.6027973,
                                            "finished_at": 1720543986.7544284
                                        }
                                    ]
                                }
                            ],
                            "failed_iteration_count": 0,
                            "total_iteration_count": 1,
                            "created_at": "2024-07-09T16:53:06",
                            "updated_at": "2024-07-09T16:53:09",
                            "contexts": {},
                            "contexts_results": [
                                {
                                    "plugin_name": "users@openstack",
                                    "plugin_cfg": {
                                        "user_choice_method": "random"
                                    },
                                    "setup": {
                                        "started_at": 1720543986.4903672,
                                        "finished_at": 1720543986.593069,
                                        "atomic_actions": [],
                                        "error": None
                                    },
                                    "cleanup": {
                                        "started_at": 1720543986.771539,
                                        "finished_at": 1720543986.7720318,
                                        "atomic_actions": [],
                                        "error": None
                                    }
                                }
                            ],
                            "position": 0,
                            "pass_sla": True,
                            "sla_results": {
                                "sla": [
                                    {
                                        "criterion": "failure_rate",
                                        "success": True,
                                        "detail": "Failure rate criteria 0.00% <= 0.00% <= 0.00% - Passed"  # noqa
                                    }
                                ]
                            },
                            "sla": {
                                "failure_rate": {
                                    "max": 0
                                }
                            }
                        }
                    ]
                }
            ]
        }
    ]
}

MEASURES_AUTHENTICATE = [
    {
        'fields': {
            'duration': 0.152,
            'failure': 0
        },
        'measurement': 'rally_task',
        'tags': {
            'component': 'keystone',
            'deployment': 'global',
            'host': 'fake',
            'scenario': 'authenticate',
            'task': 'total'
        },
        'time': '2024-07-09T16:53:05'
    },
    {
        'fields': {
            'duration': 0.152,
            'failure': 0
        },
        'measurement': 'rally_task',
        'tags': {
            'component': 'keystone',
            'deployment': 'global',
            'host': 'fake',
            'scenario': 'authenticate',
            'task': 'authenticate.keystone'
        },
        'time': '2024-07-09T16:53:05'
    }
]

MEASURES_BOOT_SERVER = [
    {
        'measurement': 'rally_task',
        'tags': {
            'deployment': 'gva_project_050',
            'component': 'nova',
            'scenario': 'boot-linux',
            'host': 'fake', 'task': 'total',
            'arg_avz_zone': 'cern-geneva-a',
            'arg_flavor_name': 'rally.tiny',
            'arg_image_name': 'Rally CirrOS 0.6.0-x86_64',
            'arg_region_name': 'next'
        },
        'time': '2024-07-09T05:55:22',
        'fields': {
            'duration': 971.248,
            'failure': 0
        }
    }, {
        'measurement': 'rally_task',
        'tags': {
            'deployment': 'gva_project_050',
            'component': 'nova',
            'scenario': 'boot-linux',
            'host': 'fake',
            'task': 'nova.boot_server',
            'arg_avz_zone': 'cern-geneva-a',
            'arg_flavor_name': 'rally.tiny',
            'arg_image_name': 'Rally CirrOS 0.6.0-x86_64',
            'arg_region_name': 'next'
        },
        'time': '2024-07-09T05:55:22',
        'fields': {
            'duration': 966.871,
            'failure': 0
        }
    }, {
        'measurement': 'rally_task',
        'tags': {
            'deployment': 'gva_project_050',
            'component': 'nova',
            'scenario': 'boot-linux',
            'host': 'fake',
            'task': 'nova.delete_server',
            'arg_avz_zone': 'cern-geneva-a',
            'arg_flavor_name': 'rally.tiny',
            'arg_image_name': 'Rally CirrOS 0.6.0-x86_64',
            'arg_region_name': 'next'
        },
        'time': '2024-07-09T05:55:22',
        'fields': {
            'duration': 4.377,
            'failure': 0
        }
    }
]


REPORT_FAIL_BOOT_SERVER = {
    "info": {
        "rally_version": "3.3.1~dev1",
        "generated_at": "2024-07-09T17:52:36",
        "format_version": "1.2"
    },
    "tasks": [
        {
            "uuid": "8a03bbc6-4370-418e-9eaf-2786992b1369",
            "title": "",
            "description": "",
            "status": "finished",
            "tags": [],
            "env_uuid": "34d9a969-b2a7-4e87-b7cb-b1e4c58124c1",
            "env_name": "gva_project_055",
            "created_at": "2024-07-09T17:18:00",
            "updated_at": "2024-07-09T17:36:23",
            "pass_sla": False,
            "subtasks": [
                {
                    "uuid": "520ba98e-8f25-4f5b-817d-db17d9e80215",
                    "title": "NovaServers.cern_boot_server_landb_ipv6ready",
                    "description": "",
                    "status": "finished",
                    "created_at": "2024-07-09T17:18:02",
                    "updated_at": "2024-07-09T17:36:23",
                    "sla": {},
                    "workloads": [
                        {
                            "uuid": "328f28c0-3769-49c4-8df4-42821fbfe7bb",
                            "description": "Create server with landb-ipv6ready=\"true\" and checks if it is set",  # noqa
                            "runner": {
                                "constant": {
                                    "times": 1,
                                    "concurrency": 1
                                }
                            },
                            "hooks": [],
                            "scenario": {
                                "NovaServers.cern_boot_server_landb_ipv6ready": {  # noqa
                                    "flavor": {
                                        "name": "rally.tiny"
                                    },
                                    "image": {
                                        "name": "Rally CirrOS 0.6.0-x86_64"
                                    },
                                    "force_delete": False,
                                    "meta": {
                                        "cern-activedirectory": "false",
                                        "cern-waitdns": "true",
                                        "landb-ipv6ready": "true"
                                    }
                                }
                            },
                            "min_duration": 1033.63,
                            "max_duration": 1033.63,
                            "start_time": 1720545482.703352,
                            "load_duration": 1033.63,
                            "full_duration": 1099.43,
                            "statistics": {
                                "durations": {
                                    "total": {
                                        "data": {
                                            "iteration_count": 1,
                                            "min": 1093.629,
                                            "median": 1093.629,
                                            "90%ile": 1093.629,
                                            "95%ile": 1093.629,
                                            "max": 1093.629,
                                            "avg": 1093.629,
                                            "success": "0.0%"
                                        },
                                        "count_per_iteration": 1,
                                        "name": "total",
                                        "display_name": "total",
                                        "children": [
                                            {
                                                "data": {
                                                    "iteration_count": 1,
                                                    "min": 1033.629,
                                                    "median": 1033.629,
                                                    "90%ile": 1033.629,
                                                    "95%ile": 1033.629,
                                                    "max": 1033.629,
                                                    "avg": 1033.629,
                                                    "success": "0.0%"
                                                },
                                                "count_per_iteration": 1,
                                                "name": "duration",
                                                "display_name": "duration",
                                                "children": []
                                            },
                                            {
                                                "data": {
                                                    "iteration_count": 1,
                                                    "min": 60.0,
                                                    "median": 60.0,
                                                    "90%ile": 60.0,
                                                    "95%ile": 60.0,
                                                    "max": 60.0,
                                                    "avg": 60.0,
                                                    "success": "0.0%"
                                                },
                                                "count_per_iteration": 1,
                                                "name": "idle_duration",
                                                "display_name": "idle_duration",  # noqa
                                                "children": []
                                            }
                                        ]
                                    },
                                    "atomics": [
                                        {
                                            "data": {
                                                "iteration_count": 1,
                                                "min": 1093.387,
                                                "median": 1093.387,
                                                "90%ile": 1093.387,
                                                "95%ile": 1093.387,
                                                "max": 1093.387,
                                                "avg": 1093.387,
                                                "success": "100.0%"
                                            },
                                            "count_per_iteration": 1,
                                            "name": "nova.boot_server",
                                            "display_name": "nova.boot_server",  # noqa
                                            "children": []
                                        },
                                        {
                                            "data": {
                                                "iteration_count": 1,
                                                "min": 0.239,
                                                "median": 0.239,
                                                "90%ile": 0.239,
                                                "95%ile": 0.239,
                                                "max": 0.239,
                                                "avg": 0.239,
                                                "success": "100.0%"
                                            },
                                            "count_per_iteration": 1,
                                            "name": "nova.get_landb_device_info",  # noqa
                                            "display_name": "nova.get_landb_device_info",  # noqa
                                            "children": []
                                        },
                                        {
                                            "data": {
                                                "iteration_count": 1,
                                                "min": 0.0,
                                                "median": 0.0,
                                                "90%ile": 0.0,
                                                "95%ile": 0.0,
                                                "max": 0.0,
                                                "avg": 0.0,
                                                "success": "100.0%"
                                            },
                                            "count_per_iteration": 1,
                                            "name": "nova.check_lanDB_ipv6",
                                            "display_name": "nova.check_lanDB_ipv6",  # noqa
                                            "children": []
                                        },
                                        {
                                            "data": {
                                                "iteration_count": 1,
                                                "min": 0.003,
                                                "median": 0.003,
                                                "90%ile": 0.003,
                                                "95%ile": 0.003,
                                                "max": 0.003,
                                                "avg": 0.003,
                                                "success": "0.0%"
                                            },
                                            "count_per_iteration": 1,
                                            "name": "nova.check_dns_resolution",  # noqa
                                            "display_name": "nova.check_dns_resolution",  # noqa
                                            "children": []
                                        }
                                    ]
                                }
                            },
                            "data": [
                                {
                                    "duration": 1033.629482269287,
                                    "timestamp": 1720545482.703352,
                                    "idle_duration": 60.0,
                                    "error": [
                                        "Exception",
                                        "DNS resolution error: No answer",
                                        "Traceback (most recent call last):\n  File \"/usr/lib/python3.9/site-packages/rally_openstack/task/scenarios/nova/utils.py\", line 1491, in _check_dns_resolution\n    answer = resolver.query(server, \"AAAA\")\n  File \"/usr/lib/python3.9/site-packages/dns/resolver.py\", line 1262, in query\n    return self.resolve(\n  File \"/usr/lib/python3.9/site-packages/dns/resolver.py\", line 1231, in resolve\n    (answer, done) = resolution.query_result(response, None)\n  File \"/usr/lib/python3.9/site-packages/dns/resolver.py\", line 768, in query_result\n    raise NoAnswer(response=answer.response)\ndns.resolver.NoAnswer: The DNS response does not contain an answer to the question: rally-328f-d6uH.cern.ch. IN AAAA\n\nDuring handling of the above exception, another exception occurred:\n\nTraceback (most recent call last):\n  File \"/usr/lib/python3.9/site-packages/rally/task/runner.py\", line 69, in _run_scenario_once\n    getattr(scenario_inst, method_name)(**scenario_kwargs)\n  File \"/usr/lib/python3.9/site-packages/rally_openstack/task/scenarios/nova/servers.py\", line 1396, in run\n    self._check_dns_resolution(server.name, landb_ipv6)\n  File \"/usr/lib/python3.9/site-packages/rally/task/atomic.py\", line 91, in func_atomic_actions\n    f = func(self, *args, **kwargs)\n  File \"/usr/lib/python3.9/site-packages/rally_openstack/task/scenarios/nova/utils.py\", line 1510, in _check_dns_resolution\n    raise Exception(\"DNS resolution error: No answer\")\nException: DNS resolution error: No answer\n"  # noqa
                                    ],
                                    "output": {
                                        "additive": [],
                                        "complete": []
                                    },
                                    "atomic_actions": [
                                        {
                                            "name": "nova.boot_server",
                                            "children": [],
                                            "started_at": 1720545482.703586,
                                            "finished_at": 1720546576.090655
                                        },
                                        {
                                            "name": "nova.get_landb_device_info",  # noqa
                                            "children": [],
                                            "started_at": 1720546576.0906801,
                                            "finished_at": 1720546576.3295612
                                        },
                                        {
                                            "name": "nova.check_lanDB_ipv6",
                                            "children": [],
                                            "started_at": 1720546576.3295732,
                                            "finished_at": 1720546576.3295758
                                        },
                                        {
                                            "name": "nova.check_dns_resolution",  # noqa
                                            "children": [],
                                            "started_at": 1720546576.32958,
                                            "finished_at": 1720546576.3328311,
                                            "failed": True
                                        }
                                    ]
                                }
                            ],
                            "failed_iteration_count": 1,
                            "total_iteration_count": 1,
                            "created_at": "2024-07-09T17:18:02",
                            "updated_at": "2024-07-09T17:36:23",
                            "contexts": {},
                            "contexts_results": [
                                {
                                    "plugin_name": "users@openstack",
                                    "plugin_cfg": {
                                        "user_choice_method": "random"
                                    },
                                    "setup": {
                                        "started_at": 1720545482.0292125,
                                        "finished_at": 1720545482.1331024,
                                        "atomic_actions": [],
                                        "error": None
                                    },
                                    "cleanup": {
                                        "started_at": 1720546581.4604146,
                                        "finished_at": 1720546581.4605103,
                                        "atomic_actions": [],
                                        "error": None
                                    }
                                },
                                {
                                    "plugin_name": "cleanup@openstack",
                                    "plugin_cfg": [
                                        "nova"
                                    ],
                                    "setup": {
                                        "started_at": 1720545482.1335273,
                                        "finished_at": 1720545482.1335285,
                                        "atomic_actions": [],
                                        "error": None
                                    },
                                    "cleanup": {
                                        "started_at": 1720546576.3554716,
                                        "finished_at": 1720546581.460087,
                                        "atomic_actions": [],
                                        "error": None
                                    }
                                }
                            ],
                            "position": 0,
                            "pass_sla": False,
                            "sla_results": {
                                "sla": [
                                    {
                                        "criterion": "failure_rate",
                                        "success": False,
                                        "detail": "Failure rate criteria 0.00% <= 100.00% <= 0.00% - Failed"  # noqa
                                    }
                                ]
                            },
                            "sla": {
                                "failure_rate": {
                                    "max": 0
                                }
                            }
                        }
                    ]
                }
            ]
        }
    ]
}

MEASURES_FAIL_BOOT_SERVER = [
    {
        'fields': {
            'duration': 1093.629,
            'failure': 1
        },
        'measurement': 'rally_task',
        'tags': {
            'component': 'nova',
            'deployment': 'gva_project_055',
            'host': 'fake',
            'scenario': 'boot_server',
            'task': 'total'
        },
        'time': '2024-07-09T17:18:00'
    },
    {
        'fields': {
            'duration': 1093.387,
            'failure': 0
        },
        'measurement': 'rally_task',
        'tags': {
            'component': 'nova',
            'deployment': 'gva_project_055',
            'host': 'fake',
            'scenario': 'boot_server',
            'task': 'nova.boot_server'
        },
        'time': '2024-07-09T17:18:00'
    },
    {
        'fields': {
            'duration': 0.239,
            'failure': 0
        },
        'measurement': 'rally_task',
        'tags': {
            'component': 'nova',
            'deployment': 'gva_project_055',
            'host': 'fake',
            'scenario': 'boot_server',
            'task': 'nova.get_landb_device_info'
        },
        'time': '2024-07-09T17:18:00'
    },
    {
        'fields': {
            'duration': 0.0,
            'failure': 0
        },
        'measurement': 'rally_task',
        'tags': {
            'component': 'nova',
            'deployment': 'gva_project_055',
            'host': 'fake',
            'scenario': 'boot_server',
            'task': 'nova.check_lanDB_ipv6'
        },
        'time': '2024-07-09T17:18:00'
    },
    {
        'fields': {
            'duration': 0.003,
            'failure': 1
        },
        'measurement': 'rally_task',
        'tags': {
            'component': 'nova',
            'deployment': 'gva_project_055',
            'host': 'fake',
            'scenario': 'boot_server',
            'task': 'nova.check_dns_resolution'
        },
        'time': '2024-07-09T17:18:00'
    }
]

REPORT_FAILURE_OCTAVIA = {
    "info": {
        "rally_version": "3.3.1~dev1",
        "generated_at": "2024-07-09T18:08:26",
        "format_version": "1.2"
    },
    "tasks": [
        {
            "uuid": "915d792e-5f23-4261-b782-fa9962d2da30",
            "title": "",
            "description": "",
            "status": "finished",
            "tags": [],
            "env_uuid": "56a8c8a5-861b-4f38-958a-eba407d7ef9b",
            "env_name": "octavia",
            "created_at": "2024-07-09T17:09:30",
            "updated_at": "2024-07-09T17:11:10",
            "pass_sla": False,
            "subtasks": [
                {
                    "uuid": "b3b1e4d3-74a7-4d3d-b3d1-ad6446034f59",
                    "title": "Octavia.cern_create_delete_l7_lb",
                    "description": "",
                    "status": "finished",
                    "created_at": "2024-07-09T17:09:31",
                    "updated_at": "2024-07-09T17:11:10",
                    "sla": {},
                    "workloads": [
                        {
                            "uuid": "9850b9f2-c0ce-41ef-8821-a52ca79c008a",
                            "description": "Create a loadbalancer per each subnet and test L7 policies on it.",  # noqa
                            "runner": {
                                "constant": {
                                    "times": 1,
                                    "concurrency": 1
                                }
                            },
                            "hooks": [],
                            "scenario": {
                                "Octavia.cern_create_delete_l7_lb": {
                                    "flavor": {
                                        "name": "rally.tiny"
                                    },
                                    "image": {
                                        "name": "Rally CirrOS 0.6.0-x86_64"
                                    },
                                    "member_args": {
                                        "username": "cirros",
                                        "password": "gocubsgo",
                                        "meta": {
                                            "cern-activedirectory": "false",
                                            "cern-waitdns": "false"
                                        },
                                        "ip_version": 4
                                    },
                                    "listener_args": {
                                        "protocol": "HTTP",
                                        "protocol_port": 80
                                    },
                                    "l7policy_args": {
                                        "action": "REDIRECT_TO_URL",
                                        "redirect_http_code": 302,
                                        "redirect_url": "http://home.cern"
                                    },
                                    "l7rule_args": {
                                        "type": "HOST_NAME",
                                        "compare_type": "EQUAL_TO",
                                        "value": "foo.bar"
                                    },
                                    "test_command": [
                                        "curl",
                                        "-v",
                                        "-H",
                                        "Host: foo.bar",
                                        "http://{}/",
                                        "|&",
                                        "grep",
                                        "home.cern"
                                    ]
                                }
                            },
                            "min_duration": 78.4444,
                            "max_duration": 78.4444,
                            "start_time": 1720544977.570208,
                            "load_duration": 78.4444,
                            "full_duration": 96.7166,
                            "statistics": {
                                "durations": {
                                    "total": {
                                        "data": {
                                            "iteration_count": 1,
                                            "min": 78.444,
                                            "median": 78.444,
                                            "90%ile": 78.444,
                                            "95%ile": 78.444,
                                            "max": 78.444,
                                            "avg": 78.444,
                                            "success": "0.0%"
                                        },
                                        "count_per_iteration": 1,
                                        "name": "total",
                                        "display_name": "total",
                                        "children": [
                                            {
                                                "data": {
                                                    "iteration_count": 1,
                                                    "min": 78.444,
                                                    "median": 78.444,
                                                    "90%ile": 78.444,
                                                    "95%ile": 78.444,
                                                    "max": 78.444,
                                                    "avg": 78.444,
                                                    "success": "0.0%"
                                                },
                                                "count_per_iteration": 1,
                                                "name": "duration",
                                                "display_name": "duration",
                                                "children": []
                                            },
                                            {
                                                "data": {
                                                    "iteration_count": 1,
                                                    "min": 0.0,
                                                    "median": 0.0,
                                                    "90%ile": 0.0,
                                                    "95%ile": 0.0,
                                                    "max": 0.0,
                                                    "avg": 0.0,
                                                    "success": "0.0%"
                                                },
                                                "count_per_iteration": 1,
                                                "name": "idle_duration",
                                                "display_name": "idle_duration",  # noqa
                                                "children": []
                                            }
                                        ]
                                    },
                                    "atomics": [
                                        {
                                            "data": {
                                                "iteration_count": 1,
                                                "min": 0.235,
                                                "median": 0.235,
                                                "90%ile": 0.235,
                                                "95%ile": 0.235,
                                                "max": 0.235,
                                                "avg": 0.235,
                                                "success": "100.0%"
                                            },
                                            "count_per_iteration": 1,
                                            "name": "octavia.flavor_list",
                                            "display_name": "octavia.flavor_list",  # noqa
                                            "children": []
                                        },
                                        {
                                            "data": {
                                                "iteration_count": 1,
                                                "min": 0.657,
                                                "median": 0.657,
                                                "90%ile": 0.657,
                                                "95%ile": 0.657,
                                                "max": 0.657,
                                                "avg": 0.657,
                                                "success": "100.0%"
                                            },
                                            "count_per_iteration": 1,
                                            "name": "neutron.list_networks",
                                            "display_name": "neutron.list_networks",  # noqa
                                            "children": []
                                        },
                                        {
                                            "data": {
                                                "iteration_count": 1,
                                                "min": 10.1,
                                                "median": 10.1,
                                                "90%ile": 10.1,
                                                "95%ile": 10.1,
                                                "max": 10.1,
                                                "avg": 10.1,
                                                "success": "100.0%"
                                            },
                                            "count_per_iteration": 2,
                                            "name": "octavia.load_balancer_create",  # noqa
                                            "display_name": "octavia.load_balancer_create (x2)",  # noqa
                                            "children": []
                                        },
                                        {
                                            "data": {
                                                "iteration_count": 1,
                                                "min": 67.303,
                                                "median": 67.303,
                                                "90%ile": 67.303,
                                                "95%ile": 67.303,
                                                "max": 67.303,
                                                "avg": 67.303,
                                                "success": "100.0%"
                                            },
                                            "count_per_iteration": 2,
                                            "name": "octavia.wait_for_loadbalancers",  # noqa
                                            "display_name": "octavia.wait_for_loadbalancers (x2)",  # noqa
                                            "children": [
                                                {
                                                    "data": {
                                                        "iteration_count": 1,
                                                        "min": 1.244,
                                                        "median": 1.244,
                                                        "90%ile": 1.244,
                                                        "95%ile": 1.244,
                                                        "max": 1.244,
                                                        "avg": 1.244,
                                                        "success": "100.0%"
                                                    },
                                                    "count_per_iteration": 35,
                                                    "name": "octavia.load_balancer_show",  # noqa
                                                    "display_name": "octavia.load_balancer_show (x35)",  # noqa
                                                    "children": []
                                                }
                                            ]
                                        },
                                        {
                                            "data": {
                                                "iteration_count": 1,
                                                "min": 0.09,
                                                "median": 0.09,
                                                "90%ile": 0.09,
                                                "95%ile": 0.09,
                                                "max": 0.09,
                                                "avg": 0.09,
                                                "success": "100.0%"
                                            },
                                            "count_per_iteration": 1,
                                            "name": "octavia.listener_create",
                                            "display_name": "octavia.listener_create",  # noqa
                                            "children": []
                                        },
                                        {
                                            "data": {
                                                "iteration_count": 1,
                                                "min": 0.059,
                                                "median": 0.059,
                                                "90%ile": 0.059,
                                                "95%ile": 0.059,
                                                "max": 0.059,
                                                "avg": 0.059,
                                                "success": "0.0%"
                                            },
                                            "count_per_iteration": 1,
                                            "name": "octavia.l7policy_create",
                                            "display_name": "octavia.l7policy_create",  # noqa
                                            "children": []
                                        }
                                    ]
                                }
                            },
                            "data": [
                                {
                                    "duration": 78.4443826675415,
                                    "timestamp": 1720544977.5702076,
                                    "idle_duration": 0.0,
                                    "error": [
                                        "OctaviaClientException",
                                        "Quota has been met for resources: L7 Policy (HTTP 403) (Request-ID: req-bf605674-d602-47ae-b48e-223b16038d85)",  # noqa
                                        "Traceback (most recent call last):\n  File \"/usr/lib/python3.9/site-packages/octaviaclient/api/v2/octavia.py\", line 34, in wrapper\n    response = func(*args, **kwargs)\n  File \"/usr/lib/python3.9/site-packages/octaviaclient/api/v2/octavia.py\", line 499, in l7policy_create\n    response = self._create(url, **kwargs)\n  File \"/usr/lib/python3.9/site-packages/osc_lib/api/api.py\", line 162, in create\n    ret = self._request(method, url, session=session, **params)\n  File \"/usr/lib/python3.9/site-packages/osc_lib/api/api.py\", line 139, in _request\n    return session.request(url, method, **kwargs)\n  File \"/usr/lib/python3.9/site-packages/keystoneauth1/session.py\", line 986, in request\n    raise exceptions.from_response(resp, method, url)\nkeystoneauth1.exceptions.http.Forbidden: Unrecognized schema in response body. (HTTP 403) (Request-ID: req-bf605674-d602-47ae-b48e-223b16038d85)\n\nThe above exception was the direct cause of the following exception:\n\nTraceback (most recent call last):\n  File \"/usr/lib/python3.9/site-packages/rally/task/runner.py\", line 69, in _run_scenario_once\n    getattr(scenario_inst, method_name)(**scenario_kwargs)\n  File \"/usr/lib/python3.9/site-packages/rally_openstack/task/scenarios/octavia/l7policy.py\", line 83, in run\n    self._populate_lb_l7(loadbalancers, listener_args,\n  File \"/usr/lib/python3.9/site-packages/rally_openstack/task/scenarios/octavia/l7policy.py\", line 59, in _populate_lb_l7\n    l7policy = self.octavia.l7policy_create(json={\n  File \"/usr/lib/python3.9/site-packages/rally/task/service.py\", line 114, in wrapper\n    return func(instance, *args, **kwargs)\n  File \"/usr/lib/python3.9/site-packages/rally/task/atomic.py\", line 91, in func_atomic_actions\n    f = func(self, *args, **kwargs)\n  File \"/usr/lib/python3.9/site-packages/rally_openstack/common/services/loadbalancer/octavia.py\", line 391, in l7policy_create\n    return self._clients.octavia().l7policy_create(**kwargs)\n  File \"/usr/lib/python3.9/site-packages/octaviaclient/api/v2/octavia.py\", line 57, in wrapper\n    raise OctaviaClientException(\noctaviaclient.api.exceptions.OctaviaClientException: Quota has been met for resources: L7 Policy (HTTP 403) (Request-ID: req-bf605674-d602-47ae-b48e-223b16038d85)\n"  # noqa
                                    ],
                                    "output": {
                                        "additive": [],
                                        "complete": []
                                    },
                                    "atomic_actions": [
                                        {
                                            "name": "octavia.flavor_list",
                                            "children": [],
                                            "started_at": 1720544977.570289,
                                            "finished_at": 1720544977.8053217
                                        },
                                        {
                                            "name": "neutron.list_networks",
                                            "children": [],
                                            "started_at": 1720544977.8053482,
                                            "finished_at": 1720544978.4620397
                                        },
                                        {
                                            "name": "octavia.load_balancer_create",  # noqa
                                            "children": [],
                                            "started_at": 1720544978.4620597,
                                            "finished_at": 1720544982.565022
                                        },
                                        {
                                            "name": "octavia.load_balancer_create",  # noqa
                                            "children": [],
                                            "started_at": 1720544982.5650353,
                                            "finished_at": 1720544988.562191
                                        },
                                        {
                                            "name": "octavia.wait_for_loadbalancers",  # noqa
                                            "children": [
                                                {
                                                    "name": "octavia.load_balancer_show",  # noqa
                                                    "children": [],
                                                    "started_at": 1720544988.562398,  # noqa
                                                    "finished_at": 1720544988.5955148  # noqa
                                                },
                                                {
                                                    "name": "octavia.load_balancer_show",  # noqa
                                                    "children": [],
                                                    "started_at": 1720544990.5975535,  # noqa
                                                    "finished_at": 1720544990.6331594  # noqa
                                                },
                                                {
                                                    "name": "octavia.load_balancer_show",  # noqa
                                                    "children": [],
                                                    "started_at": 1720544992.6349897,  # noqa
                                                    "finished_at": 1720544992.6689773  # noqa
                                                },
                                                {
                                                    "name": "octavia.load_balancer_show",  # noqa
                                                    "children": [],
                                                    "started_at": 1720544994.6708105,  # noqa
                                                    "finished_at": 1720544994.7062752  # noqa
                                                },
                                                {
                                                    "name": "octavia.load_balancer_show",  # noqa
                                                    "children": [],
                                                    "started_at": 1720544996.7080832,  # noqa
                                                    "finished_at": 1720544996.750621  # noqa
                                                },
                                                {
                                                    "name": "octavia.load_balancer_show",  # noqa
                                                    "children": [],
                                                    "started_at": 1720544998.7525506,  # noqa
                                                    "finished_at": 1720544998.789467  # noqa
                                                },
                                                {
                                                    "name": "octavia.load_balancer_show",  # noqa
                                                    "children": [],
                                                    "started_at": 1720545000.7915099,  # noqa
                                                    "finished_at": 1720545000.8222601  # noqa
                                                },
                                                {
                                                    "name": "octavia.load_balancer_show",  # noqa
                                                    "children": [],
                                                    "started_at": 1720545002.82419,  # noqa
                                                    "finished_at": 1720545002.8549082  # noqa
                                                },
                                                {
                                                    "name": "octavia.load_balancer_show",  # noqa
                                                    "children": [],
                                                    "started_at": 1720545004.8567412,  # noqa
                                                    "finished_at": 1720545004.8895683  # noqa
                                                },
                                                {
                                                    "name": "octavia.load_balancer_show",  # noqa
                                                    "children": [],
                                                    "started_at": 1720545006.8915477,  # noqa
                                                    "finished_at": 1720545006.9254243  # noqa
                                                },
                                                {
                                                    "name": "octavia.load_balancer_show",  # noqa
                                                    "children": [],
                                                    "started_at": 1720545008.9274673,  # noqa
                                                    "finished_at": 1720545008.958686  # noqa
                                                },
                                                {
                                                    "name": "octavia.load_balancer_show",  # noqa
                                                    "children": [],
                                                    "started_at": 1720545010.9606338,  # noqa
                                                    "finished_at": 1720545010.9983332  # noqa
                                                },
                                                {
                                                    "name": "octavia.load_balancer_show",  # noqa
                                                    "children": [],
                                                    "started_at": 1720545013.000004,  # noqa
                                                    "finished_at": 1720545013.0371656  # noqa
                                                },
                                                {
                                                    "name": "octavia.load_balancer_show",  # noqa
                                                    "children": [],
                                                    "started_at": 1720545015.0391476,  # noqa
                                                    "finished_at": 1720545015.0817204  # noqa
                                                },
                                                {
                                                    "name": "octavia.load_balancer_show",  # noqa
                                                    "children": [],
                                                    "started_at": 1720545017.0836194,  # noqa
                                                    "finished_at": 1720545017.1113648  # noqa
                                                },
                                                {
                                                    "name": "octavia.load_balancer_show",  # noqa
                                                    "children": [],
                                                    "started_at": 1720545019.1129506,  # noqa
                                                    "finished_at": 1720545019.1475692  # noqa
                                                },
                                                {
                                                    "name": "octavia.load_balancer_show",  # noqa
                                                    "children": [],
                                                    "started_at": 1720545021.1494684,  # noqa
                                                    "finished_at": 1720545021.1818326  # noqa
                                                },
                                                {
                                                    "name": "octavia.load_balancer_show",  # noqa
                                                    "children": [],
                                                    "started_at": 1720545023.183882,  # noqa
                                                    "finished_at": 1720545023.2140028  # noqa
                                                },
                                                {
                                                    "name": "octavia.load_balancer_show",  # noqa
                                                    "children": [],
                                                    "started_at": 1720545025.2158337,  # noqa
                                                    "finished_at": 1720545025.2513127  # noqa
                                                },
                                                {
                                                    "name": "octavia.load_balancer_show",  # noqa
                                                    "children": [],
                                                    "started_at": 1720545027.2528348,  # noqa
                                                    "finished_at": 1720545027.2875688  # noqa
                                                },
                                                {
                                                    "name": "octavia.load_balancer_show",  # noqa
                                                    "children": [],
                                                    "started_at": 1720545029.2881885,  # noqa
                                                    "finished_at": 1720545029.3207018  # noqa
                                                },
                                                {
                                                    "name": "octavia.load_balancer_show",  # noqa
                                                    "children": [],
                                                    "started_at": 1720545031.3222833,  # noqa
                                                    "finished_at": 1720545031.3564389  # noqa
                                                },
                                                {
                                                    "name": "octavia.load_balancer_show",  # noqa
                                                    "children": [],
                                                    "started_at": 1720545033.3580496,  # noqa
                                                    "finished_at": 1720545033.3929505  # noqa
                                                },
                                                {
                                                    "name": "octavia.load_balancer_show",  # noqa
                                                    "children": [],
                                                    "started_at": 1720545035.39495,  # noqa
                                                    "finished_at": 1720545035.4455855  # noqa
                                                },
                                                {
                                                    "name": "octavia.load_balancer_show",  # noqa
                                                    "children": [],
                                                    "started_at": 1720545037.447309,  # noqa
                                                    "finished_at": 1720545037.4833589  # noqa
                                                },
                                                {
                                                    "name": "octavia.load_balancer_show",  # noqa
                                                    "children": [],
                                                    "started_at": 1720545039.485019,  # noqa
                                                    "finished_at": 1720545039.5194638  # noqa
                                                },
                                                {
                                                    "name": "octavia.load_balancer_show",  # noqa
                                                    "children": [],
                                                    "started_at": 1720545041.521361,  # noqa
                                                    "finished_at": 1720545041.5608952  # noqa
                                                },
                                                {
                                                    "name": "octavia.load_balancer_show",  # noqa
                                                    "children": [],
                                                    "started_at": 1720545043.562436,  # noqa
                                                    "finished_at": 1720545043.5955338  # noqa
                                                },
                                                {
                                                    "name": "octavia.load_balancer_show",  # noqa
                                                    "children": [],
                                                    "started_at": 1720545045.5974798,  # noqa
                                                    "finished_at": 1720545045.632256  # noqa
                                                },
                                                {
                                                    "name": "octavia.load_balancer_show",  # noqa
                                                    "children": [],
                                                    "started_at": 1720545047.6341724,  # noqa
                                                    "finished_at": 1720545047.665371  # noqa
                                                },
                                                {
                                                    "name": "octavia.load_balancer_show",  # noqa
                                                    "children": [],
                                                    "started_at": 1720545049.6673784,  # noqa
                                                    "finished_at": 1720545049.6983664  # noqa
                                                },
                                                {
                                                    "name": "octavia.load_balancer_show",  # noqa
                                                    "children": [],
                                                    "started_at": 1720545051.7003825,  # noqa
                                                    "finished_at": 1720545051.733683  # noqa
                                                }
                                            ],
                                            "started_at": 1720544988.562228,
                                            "finished_at": 1720545051.7337239
                                        },
                                        {
                                            "name": "octavia.listener_create",
                                            "children": [],
                                            "started_at": 1720545051.733751,
                                            "finished_at": 1720545051.8240418
                                        },
                                        {
                                            "name": "octavia.wait_for_loadbalancers",  # noqa
                                            "children": [
                                                {
                                                    "name": "octavia.load_balancer_show",  # noqa
                                                    "children": [],
                                                    "started_at": 1720545051.8240955,  # noqa
                                                    "finished_at": 1720545051.8650239  # noqa
                                                },
                                                {
                                                    "name": "octavia.load_balancer_show",  # noqa
                                                    "children": [],
                                                    "started_at": 1720545053.865436,  # noqa
                                                    "finished_at": 1720545053.9066315  # noqa
                                                },
                                                {
                                                    "name": "octavia.load_balancer_show",  # noqa
                                                    "children": [],
                                                    "started_at": 1720545055.9086332,  # noqa
                                                    "finished_at": 1720545055.9551895  # noqa
                                                }
                                            ],
                                            "started_at": 1720545051.8240542,
                                            "finished_at": 1720545055.9552264
                                        },
                                        {
                                            "name": "octavia.l7policy_create",
                                            "children": [],
                                            "started_at": 1720545055.955255,
                                            "finished_at": 1720545056.0145705,
                                            "failed": True
                                        }
                                    ]
                                }
                            ],
                            "failed_iteration_count": 1,
                            "total_iteration_count": 1,
                            "created_at": "2024-07-09T17:09:31",
                            "updated_at": "2024-07-09T17:11:10",
                            "contexts": {},
                            "contexts_results": [
                                {
                                    "plugin_name": "users@openstack",
                                    "plugin_cfg": {
                                        "user_choice_method": "random"
                                    },
                                    "setup": {
                                        "started_at": 1720544971.3852355,
                                        "finished_at": 1720544971.4640956,
                                        "atomic_actions": [],
                                        "error": None
                                    },
                                    "cleanup": {
                                        "started_at": 1720545068.1012104,
                                        "finished_at": 1720545068.101397,
                                        "atomic_actions": [],
                                        "error": None
                                    }
                                },
                                {
                                    "plugin_name": "keypair@openstack",
                                    "plugin_cfg": {},
                                    "setup": {
                                        "started_at": 1720544971.464529,
                                        "finished_at": 1720544971.9787023,
                                        "atomic_actions": [],
                                        "error": None
                                    },
                                    "cleanup": {
                                        "started_at": 1720545066.6065984,
                                        "finished_at": 1720545068.100663,
                                        "atomic_actions": [],
                                        "error": None
                                    }
                                },
                                {
                                    "plugin_name": "allow_ssh@openstack",
                                    "plugin_cfg": None,
                                    "setup": {
                                        "started_at": 1720544971.9790568,
                                        "finished_at": 1720544976.8569636,
                                        "atomic_actions": [
                                            {
                                                "name": "neutron.list_extension",  # noqa
                                                "children": [],
                                                "started_at": 1720544971.9790933,  # noqa
                                                "finished_at": 1720544972.290729  # noqa
                                            },
                                            {
                                                "name": "neutron.create_security_group",  # noqa
                                                "children": [],
                                                "started_at": 1720544972.2926536,  # noqa
                                                "finished_at": 1720544972.5295055  # noqa
                                            },
                                            {
                                                "name": "neutron.create_security_group_rule",  # noqa
                                                "children": [],
                                                "started_at": 1720544972.5295453,  # noqa
                                                "finished_at": 1720544973.8119512  # noqa
                                            },
                                            {
                                                "name": "neutron.create_security_group_rule",  # noqa
                                                "children": [],
                                                "started_at": 1720544973.811974,  # noqa
                                                "finished_at": 1720544974.000273  # noqa
                                            },
                                            {
                                                "name": "neutron.create_security_group_rule",  # noqa
                                                "children": [],
                                                "started_at": 1720544974.0002954,  # noqa
                                                "finished_at": 1720544974.2013314  # noqa
                                            },
                                            {
                                                "name": "neutron.create_security_group_rule",  # noqa
                                                "children": [],
                                                "started_at": 1720544974.2013533,  # noqa
                                                "finished_at": 1720544975.6668255  # noqa
                                            },
                                            {
                                                "name": "neutron.create_security_group_rule",  # noqa
                                                "children": [],
                                                "started_at": 1720544975.6668472,  # noqa
                                                "finished_at": 1720544976.670106  # noqa
                                            },
                                            {
                                                "name": "neutron.create_security_group_rule",  # noqa
                                                "children": [],
                                                "started_at": 1720544976.6701317,  # noqa
                                                "finished_at": 1720544976.8552809  # noqa
                                            }
                                        ],
                                        "error": None
                                    },
                                    "cleanup": {
                                        "started_at": 1720545065.0574553,
                                        "finished_at": 1720545066.6062012,
                                        "atomic_actions": [],
                                        "error": None
                                    }
                                },
                                {
                                    "plugin_name": "cleanup@openstack",
                                    "plugin_cfg": [
                                        "nova",
                                        "octavia"
                                    ],
                                    "setup": {
                                        "started_at": 1720544976.8575947,
                                        "finished_at": 1720544976.8575962,
                                        "atomic_actions": [],
                                        "error": None
                                    },
                                    "cleanup": {
                                        "started_at": 1720545056.0400743,
                                        "finished_at": 1720545065.0570683,
                                        "atomic_actions": [],
                                        "error": None
                                    }
                                }
                            ],
                            "position": 0,
                            "pass_sla": False,
                            "sla_results": {
                                "sla": [
                                    {
                                        "criterion": "failure_rate",
                                        "success": False,
                                        "detail": "Failure rate criteria 0.00% <= 100.00% <= 0.00% - Failed"  # noqa
                                    }
                                ]
                            },
                            "sla": {
                                "failure_rate": {
                                    "max": 0
                                }
                            }
                        }
                    ]
                }
            ]
        }
    ]
}

MEASURES_FAILURE_OCTAVIA = [
    {
        'fields': {
            'duration': 78.444,
            'failure': 1
        },
        'measurement': 'rally_task',
        'tags': {
            'component': 'octavia',
            'deployment': 'octavia',
            'host': 'fake',
            'scenario': 'l7-lb-redirect',
            'task': 'total'
        },
        'time': '2024-07-09T17:09:30'
    },
    {
        'fields': {
            'duration': 0.235,
            'failure': 0
        },
        'measurement': 'rally_task',
        'tags': {
            'component': 'octavia',
            'deployment': 'octavia',
            'host': 'fake',
            'scenario': 'l7-lb-redirect',
            'task': 'octavia.flavor_list'
        },
        'time': '2024-07-09T17:09:30'
    },
    {
        'fields': {
            'duration': 0.657,
            'failure': 0
        },
        'measurement': 'rally_task',
        'tags': {
            'component': 'octavia',
            'deployment': 'octavia',
            'host': 'fake',
            'scenario': 'l7-lb-redirect',
            'task': 'neutron.list_networks'
        },
        'time': '2024-07-09T17:09:30'
    },
    {
        'fields': {
            'duration': 10.1,
            'failure': 0
        },
        'measurement': 'rally_task',
        'tags': {
            'component': 'octavia',
            'deployment': 'octavia',
            'host': 'fake',
            'scenario': 'l7-lb-redirect',
            'task': 'octavia.load_balancer_create'
        },
        'time': '2024-07-09T17:09:30'
    },
    {
        'fields': {
            'duration': 67.303,
            'failure': 0
        },
        'measurement': 'rally_task',
        'tags': {
            'component': 'octavia',
            'deployment': 'octavia',
            'host': 'fake',
            'scenario': 'l7-lb-redirect',
            'task': 'octavia.wait_for_loadbalancers'
        },
        'time': '2024-07-09T17:09:30'
    },
    {
        'fields': {
            'duration': 0.09,
            'failure': 0
        },
        'measurement': 'rally_task',
        'tags': {
            'component': 'octavia',
            'deployment': 'octavia',
            'host': 'fake',
            'scenario': 'l7-lb-redirect',
            'task': 'octavia.listener_create'
        },
        'time': '2024-07-09T17:09:30'
    },
    {
        'fields': {
            'duration': 0.059,
            'failure': 1
        },
        'measurement': 'rally_task',
        'tags': {
            'component': 'octavia',
            'deployment': 'octavia',
            'host': 'fake',
            'scenario': 'l7-lb-redirect',
            'task': 'octavia.l7policy_create'
        },
        'time': '2024-07-09T17:09:30'
    }
]

REPORT_BOOT_SNAPSHOT = {
    "info": {
        "generated_at": "2020-04-15T14:47:42",
        "rally_version": "2.0.0",
        "format_version": "1.2"
    },
    "tasks": [
        {
            "uuid": "2d2b92ec-d461-499d-a5d3-003e3aef0fec",
            "title": "",
            "description": "",
            "status": "finished",
            "tags": [],
            "env_uuid": "45388dfe-ea04-4917-9eff-06577b83c1ca",
            "env_name": "gva_project_043",
            "created_at": "2020-04-15T14:18:06",
            "updated_at": "2020-04-15T14:19:55",
            "pass_sla": True,
            "subtasks": [
                {
                    "uuid": "546b70c6-013d-4ede-8f48-b0b5f2e52e0b",
                    "title": "NovaServers.snapshot_server",
                    "description": "",
                    "status": "finished",
                    "created_at": "2020-04-15T14:18:08",
                    "updated_at": "2020-04-15T14:19:55",
                    "sla": {},
                    "workloads": [
                        {
                            "uuid": "80df8c7e-ea3f-466b-902a-ce15d5c42300",
                            "description": "Boot a server,make its snapshot and delete both.",  # noqa
                            "runner": {
                                "constant": {
                                    "concurrency": 1,
                                    "times": 1
                                }
                            },
                            "hooks": [],
                            "scenario": {
                                "NovaServers.snapshot_server": {
                                    "force_delete": False,
                                    "flavor": {
                                        "name": "rally.nano"
                                    },
                                    "meta": {
                                        "cern-services": "false"
                                    },
                                    "image": {
                                        "name": "Rally CirrOS IPv6"
                                    }
                                }
                            },
                            "min_duration": 82.0101,
                            "max_duration": 82.0101,
                            "start_time": 1586960289.37152,
                            "load_duration": 82.0101,
                            "full_duration": 104.601,
                            "statistics": {
                                "durations": {
                                    "total": {
                                        "data": {
                                            "success": "100.0%",
                                            "min": 102.01,
                                            "max": 102.01,
                                            "median": 102.01,
                                            "95%ile": 102.01,
                                            "iteration_count": 1,
                                            "avg": 102.01,
                                            "90%ile": 102.01
                                        },
                                        "display_name": "total",
                                        "name": "total",
                                        "count_per_iteration": 1,
                                        "children": [
                                            {
                                                "data": {
                                                    "success": "100.0%",
                                                    "min": 82.01,
                                                    "max": 82.01,
                                                    "median": 82.01,
                                                    "95%ile": 82.01,
                                                    "iteration_count": 1,
                                                    "avg": 82.01,
                                                    "90%ile": 82.01
                                                },
                                                "display_name": "duration",
                                                "name": "duration",
                                                "count_per_iteration": 1,
                                                "children": []
                                            },
                                            {
                                                "data": {
                                                    "success": "100.0%",
                                                    "min": 20.0,
                                                    "max": 20.0,
                                                    "median": 20.0,
                                                    "95%ile": 20.0,
                                                    "iteration_count": 1,
                                                    "avg": 20.0,
                                                    "90%ile": 20.0
                                                },
                                                "display_name": "idle_duration",  # noqa
                                                "name": "idle_duration",
                                                "count_per_iteration": 1,
                                                "children": []
                                            }
                                        ]
                                    },
                                    "atomics": [
                                        {
                                            "data": {
                                                "success": "100.0%",
                                                "min": 71.876,
                                                "max": 71.876,
                                                "median": 71.876,
                                                "95%ile": 71.876,
                                                "iteration_count": 1,
                                                "avg": 71.876,
                                                "90%ile": 71.876
                                            },
                                            "display_name": "nova.boot_server (x2)",  # noqa
                                            "name": "nova.boot_server",
                                            "count_per_iteration": 2,
                                            "children": []
                                        },
                                        {
                                            "data": {
                                                "success": "100.0%",
                                                "min": 10.226,
                                                "max": 10.226,
                                                "median": 10.226,
                                                "95%ile": 10.226,
                                                "iteration_count": 1,
                                                "avg": 10.226,
                                                "90%ile": 10.226
                                            },
                                            "display_name": "nova.snapshot_server",  # noqa
                                            "name": "nova.snapshot_server",
                                            "count_per_iteration": 1,
                                            "children": [
                                                {
                                                    "data": {
                                                        "success": "100.0%",
                                                        "min": 0.312,
                                                        "max": 0.312,
                                                        "median": 0.312,
                                                        "95%ile": 0.312,
                                                        "iteration_count": 1,
                                                        "avg": 0.312,
                                                        "90%ile": 0.312
                                                    },
                                                    "display_name": "glance_v2.get_image",  # noqa
                                                    "name": "glance_v2.get_image",  # noqa
                                                    "count_per_iteration": 1,
                                                    "children": []
                                                },
                                                {
                                                    "data": {
                                                        "success": "100.0%",
                                                        "min": 8.472,
                                                        "max": 8.472,
                                                        "median": 8.472,
                                                        "95%ile": 8.472,
                                                        "iteration_count": 1,
                                                        "avg": 8.472,
                                                        "90%ile": 8.472
                                                    },
                                                    "display_name": "glance.wait_for_image",  # noqa
                                                    "name": "glance.wait_for_image",  # noqa
                                                    "count_per_iteration": 1,
                                                    "children": [
                                                        {
                                                            "data": {
                                                                "success": "100.0%",  # noqa
                                                                "min": 0.465,
                                                                "max": 0.465,
                                                                "median": 0.465,  # noqa
                                                                "95%ile": 0.465,  # noqa
                                                                "iteration_count": 1,  # noqa
                                                                "avg": 0.465,
                                                                "90%ile": 0.465
                                                            },
                                                            "display_name": "glance_v2.get_image (x5)",  # noqa
                                                            "name": "glance_v2.get_image",  # noqa
                                                            "count_per_iteration": 5,  # noqa
                                                            "children": []
                                                        }
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            "data": {
                                                "success": "100.0%",
                                                "min": 18.056,
                                                "max": 18.056,
                                                "median": 18.056,
                                                "95%ile": 18.056,
                                                "iteration_count": 1,
                                                "avg": 18.056,
                                                "90%ile": 18.056
                                            },
                                            "display_name": "nova.delete_server (x2)",  # noqa
                                            "name": "nova.delete_server",
                                            "count_per_iteration": 2,
                                            "children": []
                                        },
                                        {
                                            "data": {
                                                "success": "100.0%",
                                                "min": 1.851,
                                                "max": 1.851,
                                                "median": 1.851,
                                                "95%ile": 1.851,
                                                "iteration_count": 1,
                                                "avg": 1.851,
                                                "90%ile": 1.851
                                            },
                                            "display_name": "nova.delete_image",  # noqa
                                            "name": "nova.delete_image",
                                            "count_per_iteration": 1,
                                            "children": [
                                                {
                                                    "data": {
                                                        "success": "100.0%",
                                                        "min": 1.829,
                                                        "max": 1.829,
                                                        "median": 1.829,
                                                        "95%ile": 1.829,
                                                        "iteration_count": 1,
                                                        "avg": 1.829,
                                                        "90%ile": 1.829
                                                    },
                                                    "display_name": "glance_v2.delete_image",  # noqa
                                                    "name": "glance_v2.delete_image",  # noqa
                                                    "count_per_iteration": 1,
                                                    "children": []
                                                },
                                                {
                                                    "data": {
                                                        "success": "100.0%",
                                                        "min": 0.021,
                                                        "max": 0.021,
                                                        "median": 0.021,
                                                        "95%ile": 0.021,
                                                        "iteration_count": 1,
                                                        "avg": 0.021,
                                                        "90%ile": 0.021
                                                    },
                                                    "display_name": "glance.wait_for_delete",  # noqa
                                                    "name": "glance.wait_for_delete",  # noqa
                                                    "count_per_iteration": 1,
                                                    "children": [
                                                        {
                                                            "data": {
                                                                "success": "0.0%",  # noqa
                                                                "min": 0.02,
                                                                "max": 0.02,
                                                                "median": 0.02,
                                                                "95%ile": 0.02,
                                                                "iteration_count": 1,  # noqa
                                                                "avg": 0.02,
                                                                "90%ile": 0.02
                                                            },
                                                            "display_name": "glance_v2.get_image",  # noqa
                                                            "name": "glance_v2.get_image",  # noqa
                                                            "count_per_iteration": 1,  # noqa
                                                            "children": []
                                                        }
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                }
                            },
                            "data": [
                                {
                                    "timestamp": 1586960289.371515,
                                    "error": [],
                                    "duration": 82.01013803482056,
                                    "output": {
                                        "additive": [],
                                        "complete": []
                                    },
                                    "idle_duration": 20.0,
                                    "atomic_actions": [
                                        {
                                            "finished_at": 1586960325.872227,
                                            "started_at": 1586960289.371644,
                                            "name": "nova.boot_server",
                                            "children": []
                                        },
                                        {
                                            "finished_at": 1586960336.097965,
                                            "started_at": 1586960325.872269,
                                            "name": "nova.snapshot_server",
                                            "children": [
                                                {
                                                    "finished_at": 1586960327.626062,  # noqa
                                                    "started_at": 1586960327.313967,  # noqa
                                                    "name": "glance_v2.get_image",  # noqa
                                                    "children": []
                                                },
                                                {
                                                    "finished_at": 1586960336.097957,  # noqa
                                                    "started_at": 1586960327.626398,  # noqa
                                                    "name": "glance.wait_for_image",  # noqa
                                                    "children": [
                                                        {
                                                            "finished_at": 1586960327.652593,  # noqa
                                                            "started_at": 1586960327.626562,  # noqa
                                                            "name": "glance_v2.get_image",  # noqa
                                                            "children": []
                                                        },
                                                        {
                                                            "finished_at": 1586960329.789069,  # noqa
                                                            "started_at": 1586960329.653554,  # noqa
                                                            "name": "glance_v2.get_image",  # noqa
                                                            "children": []
                                                        },
                                                        {
                                                            "finished_at": 1586960331.924673,  # noqa
                                                            "started_at": 1586960331.791179,  # noqa
                                                            "name": "glance_v2.get_image",  # noqa
                                                            "children": []
                                                        },
                                                        {
                                                            "finished_at": 1586960333.968254,  # noqa
                                                            "started_at": 1586960333.926959,  # noqa
                                                            "name": "glance_v2.get_image",  # noqa
                                                            "children": []
                                                        },
                                                        {
                                                            "finished_at": 1586960336.09776,  # noqa
                                                            "started_at": 1586960335.968997,  # noqa
                                                            "name": "glance_v2.get_image",  # noqa
                                                            "children": []
                                                        }
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            "finished_at": 1586960343.114197,
                                            "started_at": 1586960336.098087,
                                            "name": "nova.delete_server",
                                            "children": []
                                        },
                                        {
                                            "finished_at": 1586960378.490133,
                                            "started_at": 1586960343.114219,
                                            "name": "nova.boot_server",
                                            "children": []
                                        },
                                        {
                                            "finished_at": 1586960389.530177,
                                            "started_at": 1586960378.490213,
                                            "name": "nova.delete_server",
                                            "children": []
                                        },
                                        {
                                            "finished_at": 1586960391.381631,
                                            "started_at": 1586960389.530217,
                                            "name": "nova.delete_image",
                                            "children": [
                                                {
                                                    "finished_at": 1586960391.360671,  # noqa
                                                    "started_at": 1586960389.531711,  # noqa
                                                    "name": "glance_v2.delete_image",  # noqa
                                                    "children": []
                                                },
                                                {
                                                    "finished_at": 1586960391.38162,  # noqa
                                                    "started_at": 1586960391.361062,  # noqa
                                                    "name": "glance.wait_for_delete",  # noqa
                                                    "children": [
                                                        {
                                                            "finished_at": 1586960391.381569,  # noqa
                                                            "started_at": 1586960391.361226,  # noqa
                                                            "children": [],
                                                            "failed": True,
                                                            "name": "glance_v2.get_image"  # noqa
                                                        }
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ],
                            "failed_iteration_count": 0,
                            "total_iteration_count": 1,
                            "created_at": "2020-04-15T14:18:08",
                            "updated_at": "2020-04-15T14:19:55",
                            "contexts": {},
                            "contexts_results": [
                                {
                                    "setup": {
                                        "finished_at": 1586960288.529602,
                                        "started_at": 1586960288.373862,
                                        "atomic_actions": [],
                                        "error": None
                                    },
                                    "cleanup": {
                                        "finished_at": 1586960392.972025,
                                        "started_at": 1586960392.97168,
                                        "atomic_actions": [],
                                        "error": None
                                    },
                                    "plugin_cfg": {
                                        "user_choice_method": "random"
                                    },
                                    "plugin_name": "users@openstack"
                                },
                                {
                                    "setup": {
                                        "finished_at": 1586960288.530461,
                                        "started_at": 1586960288.530457,
                                        "atomic_actions": [],
                                        "error": None
                                    },
                                    "cleanup": {
                                        "finished_at": 1586960392.970757,
                                        "started_at": 1586960391.410761,
                                        "atomic_actions": [],
                                        "error": None
                                    },
                                    "plugin_cfg": [
                                        "nova",
                                        "glance"
                                    ],
                                    "plugin_name": "cleanup@openstack"
                                }
                            ],
                            "position": 0,
                            "pass_sla": True,
                            "sla_results": {
                                "sla": [
                                    {
                                        "criterion": "failure_rate",
                                        "detail": "Failure rate criteria 0.00% <= 0.00% <= 0.00% - Passed",  # noqa
                                        "success": True
                                    }
                                ]
                            },
                            "sla": {
                                "failure_rate": {
                                    "max": 0
                                }
                            }
                        }
                    ]
                }
            ]
        }
    ]
}

MEASURES_BOOT_SNAPSHOT = [
    {
        "measurement": "rally_task",
        "tags": {
            "deployment": "gva_project_043",
            "component": "nova",
            'host': 'fake',
            "scenario": "boot_snapshot",
            "task": "total"
        },
        "time": "2020-04-15T14:18:06",
        "fields": {
            "duration": 102.01,
            "failure": 0
        }
    },
    {
        "measurement": "rally_task",
        "tags": {
            "deployment": "gva_project_043",
            "component": "nova",
            'host': 'fake',
            "scenario": "boot_snapshot",
            "task": "nova.boot_server"
        },
        "time": "2020-04-15T14:18:06",
        "fields": {
            "duration": 71.876,
            "failure": 0
        }
    },
    {
        "measurement": "rally_task",
        "tags": {
            "deployment": "gva_project_043",
            "component": "nova",
            'host': 'fake',
            "scenario": "boot_snapshot",
            "task": "nova.snapshot_server"
        },
        "time": "2020-04-15T14:18:06",
        "fields": {
            "duration": 10.226,
            "failure": 0
        }
    },
    {
        "measurement": "rally_task",
        "tags": {
            "deployment": "gva_project_043",
            "component": "nova",
            'host': 'fake',
            "scenario": "boot_snapshot",
            "task": "nova.delete_server"
        },
        "time": "2020-04-15T14:18:06",
        "fields": {
            "duration": 18.056,
            "failure": 0
        }
    },
    {
        "measurement": "rally_task",
        "tags": {
            "deployment": "gva_project_043",
            "component": "nova",
            'host': 'fake',
            "scenario": "boot_snapshot",
            "task": "nova.delete_image"
        },
        "time": "2020-04-15T14:18:06",
        "fields": {
            "duration": 1.851,
            "failure": 0
        }
    }
]


REPORT_BOOT_VOLUME = {
    "info": {
        "generated_at": "2020-04-15T14:48:49",
        "rally_version": "2.0.0",
        "format_version": "1.2"
    },
    "tasks": [
        {
            "uuid": "4e6fc452-618a-4d62-b990-007ca3ec69d7",
            "title": "",
            "description": "",
            "status": "finished",
            "tags": [],
            "env_uuid": "c6c6e672-bbbc-4c5d-adcd-b4afdc4f40aa",
            "env_name": "gva_project_046",
            "created_at": "2020-04-15T13:29:45",
            "updated_at": "2020-04-15T13:44:01",
            "pass_sla": True,
            "subtasks": [
                {
                    "uuid": "58f26b08-808e-4ac6-9324-b249263daf63",
                    "title": "NovaServers.boot_server_from_volume_and_delete",
                    "description": "",
                    "status": "finished",
                    "created_at": "2020-04-15T13:29:48",
                    "updated_at": "2020-04-15T13:44:01",
                    "sla": {},
                    "workloads": [
                        {
                            "uuid": "65b6c201-6071-45d0-9e48-5e85f75c5f22",
                            "description": "Boot a server from volume and then delete it.",  # noqa
                            "runner": {
                                "constant": {
                                    "concurrency": 1,
                                    "times": 1
                                }
                            },
                            "hooks": [],
                            "scenario": {
                                "NovaServers.boot_server_from_volume_and_delete": {  # noqa
                                    "volume_size": 1,
                                    "flavor": {
                                        "name": "rally.nano"
                                    },
                                    "image": {
                                        "name": "Rally CirrOS IPv6"
                                    },
                                    "force_delete": False
                                }
                            },
                            "min_duration": 836.236,
                            "max_duration": 836.236,
                            "start_time": 1586957388.62729,
                            "load_duration": 836.236,
                            "full_duration": 851.89,
                            "statistics": {
                                "durations": {
                                    "total": {
                                        "data": {
                                            "success": "100.0%",
                                            "min": 846.236,
                                            "max": 846.236,
                                            "median": 846.236,
                                            "95%ile": 846.236,
                                            "iteration_count": 1,
                                            "avg": 846.236,
                                            "90%ile": 846.236
                                        },
                                        "display_name": "total",
                                        "name": "total",
                                        "count_per_iteration": 1,
                                        "children": [
                                            {
                                                "data": {
                                                    "success": "100.0%",
                                                    "min": 836.236,
                                                    "max": 836.236,
                                                    "median": 836.236,
                                                    "95%ile": 836.236,
                                                    "iteration_count": 1,
                                                    "avg": 836.236,
                                                    "90%ile": 836.236
                                                },
                                                "display_name": "duration",
                                                "name": "duration",
                                                "count_per_iteration": 1,
                                                "children": []
                                            },
                                            {
                                                "data": {
                                                    "success": "100.0%",
                                                    "min": 10.0,
                                                    "max": 10.0,
                                                    "median": 10.0,
                                                    "95%ile": 10.0,
                                                    "iteration_count": 1,
                                                    "avg": 10.0,
                                                    "90%ile": 10.0
                                                },
                                                "display_name": "idle_duration",  # noqa
                                                "name": "idle_duration",
                                                "count_per_iteration": 1,
                                                "children": []
                                            }
                                        ]
                                    },
                                    "atomics": [
                                        {
                                            "data": {
                                                "success": "100.0%",
                                                "min": 4.01,
                                                "max": 4.01,
                                                "median": 4.01,
                                                "95%ile": 4.01,
                                                "iteration_count": 1,
                                                "avg": 4.01,
                                                "90%ile": 4.01
                                            },
                                            "display_name": "cinder_v3.create_volume",  # noqa
                                            "name": "cinder_v3.create_volume",
                                            "count_per_iteration": 1,
                                            "children": []
                                        },
                                        {
                                            "data": {
                                                "success": "100.0%",
                                                "min": 832.998,
                                                "max": 832.998,
                                                "median": 832.998,
                                                "95%ile": 832.998,
                                                "iteration_count": 1,
                                                "avg": 832.998,
                                                "90%ile": 832.998
                                            },
                                            "display_name": "nova.boot_server",
                                            "name": "nova.boot_server",
                                            "count_per_iteration": 1,
                                            "children": []
                                        },
                                        {
                                            "data": {
                                                "success": "100.0%",
                                                "min": 9.227,
                                                "max": 9.227,
                                                "median": 9.227,
                                                "95%ile": 9.227,
                                                "iteration_count": 1,
                                                "avg": 9.227,
                                                "90%ile": 9.227
                                            },
                                            "display_name": "nova.delete_server",  # noqa
                                            "name": "nova.delete_server",
                                            "count_per_iteration": 1,
                                            "children": []
                                        }
                                    ]
                                }
                            },
                            "data": [
                                {
                                    "timestamp": 1586957388.627287,
                                    "error": [],
                                    "duration": 836.2360911369324,
                                    "output": {
                                        "additive": [],
                                        "complete": []
                                    },
                                    "idle_duration": 10.0,
                                    "atomic_actions": [
                                        {
                                            "finished_at": 1586957392.637694,
                                            "started_at": 1586957388.627484,
                                            "name": "cinder_v3.create_volume",
                                            "children": []
                                        },
                                        {
                                            "finished_at": 1586958225.635509,
                                            "started_at": 1586957392.637844,
                                            "name": "nova.boot_server",
                                            "children": []
                                        },
                                        {
                                            "finished_at": 1586958234.86329,
                                            "started_at": 1586958225.636222,
                                            "name": "nova.delete_server",
                                            "children": []
                                        }
                                    ]
                                }
                            ],
                            "failed_iteration_count": 0,
                            "total_iteration_count": 1,
                            "created_at": "2020-04-15T13:29:48",
                            "updated_at": "2020-04-15T13:44:01",
                            "contexts": {},
                            "contexts_results": [
                                {
                                    "setup": {
                                        "finished_at": 1586957387.69575,
                                        "started_at": 1586957387.599061,
                                        "atomic_actions": [],
                                        "error": None
                                    },
                                    "cleanup": {
                                        "finished_at": 1586958239.487495,
                                        "started_at": 1586958239.486933,
                                        "atomic_actions": [],
                                        "error": None
                                    },
                                    "plugin_cfg": {
                                        "user_choice_method": "random"
                                    },
                                    "plugin_name": "users@openstack"
                                },
                                {
                                    "setup": {
                                        "finished_at": 1586957387.696666,
                                        "started_at": 1586957387.696663,
                                        "atomic_actions": [],
                                        "error": None
                                    },
                                    "cleanup": {
                                        "finished_at": 1586958239.48598,
                                        "started_at": 1586958234.890669,
                                        "atomic_actions": [],
                                        "error": None
                                    },
                                    "plugin_cfg": [
                                        "nova",
                                        "cinder"
                                    ],
                                    "plugin_name": "cleanup@openstack"
                                }
                            ],
                            "position": 0,
                            "pass_sla": True,
                            "sla_results": {
                                "sla": [
                                    {
                                        "criterion": "failure_rate",
                                        "detail": "Failure rate criteria 0.00% <= 0.00% <= 0.00% - Passed",  # noqa
                                        "success": True
                                    }
                                ]
                            },
                            "sla": {
                                "failure_rate": {
                                    "max": 0
                                }
                            }
                        }
                    ]
                }
            ]
        }
    ]
}

MEASURES_BOOT_VOLUME = [
    {
        "measurement": "rally_task",
        "tags": {
            "deployment": "gva_project_046",
            "component": "nova",
            'host': 'fake',
            "scenario": "boot_volume",
            "task": "total"
        },
        "time": "2020-04-15T13:29:45",
        "fields": {
            "duration": 846.236,
            "failure": 0
        }
    },
    {
        "measurement": "rally_task",
        "tags": {
            "deployment": "gva_project_046",
            "component": "nova",
            'host': 'fake',
            "scenario": "boot_volume",
            "task": "cinder_v3.create_volume"
        },
        "time": "2020-04-15T13:29:45",
        "fields": {
            "duration": 4.01,
            "failure": 0
        }
    },
    {
        "measurement": "rally_task",
        "tags": {
            "deployment": "gva_project_046",
            "component": "nova",
            'host': 'fake',
            "scenario": "boot_volume",
            "task": "nova.boot_server"
        },
        "time": "2020-04-15T13:29:45",
        "fields": {
            "duration": 832.998,
            "failure": 0
        }
    },
    {
        "measurement": "rally_task",
        "tags": {
            "deployment": "gva_project_046",
            "component": "nova",
            'host': 'fake',
            "scenario": "boot_volume",
            "task": "nova.delete_server"
        },
        "time": "2020-04-15T13:29:45",
        "fields": {
            "duration": 9.227,
            "failure": 0
        }
    }
]

REPORT_SNAPSHOT_SERVER = {
    "info": {
        "generated_at": "2020-04-15T14:42:14",
        "rally_version": "2.0.0",
        "format_version": "1.2"
    },
    "tasks": [
        {
            "uuid": "74f921ab-bcbc-4909-8bec-955f6877958d",
            "title": "",
            "description": "",
            "status": "finished",
            "tags": [],
            "env_uuid": "0ce77670-0164-4040-b691-a1d3965b13c0",
            "env_name": "crit_project_001",
            "created_at": "2020-04-13T19:50:08",
            "updated_at": "2020-04-13T19:51:33",
            "pass_sla": True,
            "subtasks": [
                {
                    "uuid": "e7d02303-02d6-48ef-a309-e368caa7ccd4",
                    "title": "NovaServers.snapshot_server",
                    "description": "",
                    "status": "finished",
                    "created_at": "2020-04-13T19:50:10",
                    "updated_at": "2020-04-13T19:51:33",
                    "sla": {},
                    "workloads": [
                        {
                            "uuid": "9385c76a-e7fb-4103-8b1a-cebf3a31bd24",
                            "description": "Boot a server,make its snapshot and delete both.",  # noqa
                            "runner": {
                                "constant": {
                                    "concurrency": 1,
                                    "times": 1
                                }
                            },
                            "hooks": [],
                            "scenario": {
                                "NovaServers.snapshot_server": {
                                    "force_delete": False,
                                    "flavor": {
                                        "name": "rally.nano"
                                    },
                                    "meta": {
                                        "cern-services": "false"
                                    },
                                    "image": {
                                        "name": "Rally CirrOS IPv6"
                                    }
                                }
                            },
                            "min_duration": 58.3512,
                            "max_duration": 58.3512,
                            "start_time": 1586807411.44234,
                            "load_duration": 58.3512,
                            "full_duration": 80.5958,
                            "statistics": {
                                "durations": {
                                    "total": {
                                        "data": {
                                            "success": "100.0%",
                                            "min": 78.351,
                                            "max": 78.351,
                                            "median": 78.351,
                                            "95%ile": 78.351,
                                            "iteration_count": 1,
                                            "avg": 78.351,
                                            "90%ile": 78.351
                                        },
                                        "display_name": "total",
                                        "name": "total",
                                        "count_per_iteration": 1,
                                        "children": [
                                            {
                                                "data": {
                                                    "success": "100.0%",
                                                    "min": 58.351,
                                                    "max": 58.351,
                                                    "median": 58.351,
                                                    "95%ile": 58.351,
                                                    "iteration_count": 1,
                                                    "avg": 58.351,
                                                    "90%ile": 58.351
                                                },
                                                "display_name": "duration",
                                                "name": "duration",
                                                "count_per_iteration": 1,
                                                "children": []
                                            },
                                            {
                                                "data": {
                                                    "success": "100.0%",
                                                    "min": 20.0,
                                                    "max": 20.0,
                                                    "median": 20.0,
                                                    "95%ile": 20.0,
                                                    "iteration_count": 1,
                                                    "avg": 20.0,
                                                    "90%ile": 20.0
                                                },
                                                "display_name": "idle_duration",  # noqa
                                                "name": "idle_duration",
                                                "count_per_iteration": 1,
                                                "children": []
                                            }
                                        ]
                                    },
                                    "atomics": [
                                        {
                                            "data": {
                                                "success": "100.0%",
                                                "min": 49.191,
                                                "max": 49.191,
                                                "median": 49.191,
                                                "95%ile": 49.191,
                                                "iteration_count": 1,
                                                "avg": 49.191,
                                                "90%ile": 49.191
                                            },
                                            "display_name": "nova.boot_server (x2)",  # noqa
                                            "name": "nova.boot_server",
                                            "count_per_iteration": 2,
                                            "children": []
                                        },
                                        {
                                            "data": {
                                                "success": "100.0%",
                                                "min": 11.702,
                                                "max": 11.702,
                                                "median": 11.702,
                                                "95%ile": 11.702,
                                                "iteration_count": 1,
                                                "avg": 11.702,
                                                "90%ile": 11.702
                                            },
                                            "display_name": "nova.snapshot_server",  # noqa
                                            "name": "nova.snapshot_server",
                                            "count_per_iteration": 1,
                                            "children": [
                                                {
                                                    "data": {
                                                        "success": "100.0%",
                                                        "min": 0.213,
                                                        "max": 0.213,
                                                        "median": 0.213,
                                                        "95%ile": 0.213,
                                                        "iteration_count": 1,
                                                        "avg": 0.213,
                                                        "90%ile": 0.213
                                                    },
                                                    "display_name": "glance_v2.get_image",  # noqa
                                                    "name": "glance_v2.get_image",  # noqa
                                                    "count_per_iteration": 1,
                                                    "children": []
                                                },
                                                {
                                                    "data": {
                                                        "success": "100.0%",
                                                        "min": 10.578,
                                                        "max": 10.578,
                                                        "median": 10.578,
                                                        "95%ile": 10.578,
                                                        "iteration_count": 1,
                                                        "avg": 10.578,
                                                        "90%ile": 10.578
                                                    },
                                                    "display_name": "glance.wait_for_image",  # noqa
                                                    "name": "glance.wait_for_image",  # noqa
                                                    "count_per_iteration": 1,
                                                    "children": [
                                                        {
                                                            "data": {
                                                                "success": "100.0%",  # noqa
                                                                "min": 0.572,
                                                                "max": 0.572,
                                                                "median": 0.572,  # noqa
                                                                "95%ile": 0.572,  # noqa
                                                                "iteration_count": 1,  # noqa
                                                                "avg": 0.572,
                                                                "90%ile": 0.572
                                                            },
                                                            "display_name": "glance_v2.get_image (x6)",  # noqa
                                                            "name": "glance_v2.get_image",  # noqa
                                                            "count_per_iteration": 6,  # noqa
                                                            "children": []
                                                        }
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            "data": {
                                                "success": "100.0%",
                                                "min": 15.602,
                                                "max": 15.602,
                                                "median": 15.602,
                                                "95%ile": 15.602,
                                                "iteration_count": 1,
                                                "avg": 15.602,
                                                "90%ile": 15.602
                                            },
                                            "display_name": "nova.delete_server (x2)",  # noqa
                                            "name": "nova.delete_server",
                                            "count_per_iteration": 2,
                                            "children": []
                                        },
                                        {
                                            "data": {
                                                "success": "100.0%",
                                                "min": 1.855,
                                                "max": 1.855,
                                                "median": 1.855,
                                                "95%ile": 1.855,
                                                "iteration_count": 1,
                                                "avg": 1.855,
                                                "90%ile": 1.855
                                            },
                                            "display_name": "nova.delete_image",  # noqa
                                            "name": "nova.delete_image",
                                            "count_per_iteration": 1,
                                            "children": [
                                                {
                                                    "data": {
                                                        "success": "100.0%",
                                                        "min": 1.74,
                                                        "max": 1.74,
                                                        "median": 1.74,
                                                        "95%ile": 1.74,
                                                        "iteration_count": 1,
                                                        "avg": 1.74,
                                                        "90%ile": 1.74
                                                    },
                                                    "display_name": "glance_v2.delete_image",  # noqa
                                                    "name": "glance_v2.delete_image",  # noqa
                                                    "count_per_iteration": 1,
                                                    "children": []
                                                },
                                                {
                                                    "data": {
                                                        "success": "100.0%",
                                                        "min": 0.114,
                                                        "max": 0.114,
                                                        "median": 0.114,
                                                        "95%ile": 0.114,
                                                        "iteration_count": 1,
                                                        "avg": 0.114,
                                                        "90%ile": 0.114
                                                    },
                                                    "display_name": "glance.wait_for_delete",  # noqa
                                                    "name": "glance.wait_for_delete",  # noqa
                                                    "count_per_iteration": 1,
                                                    "children": [
                                                        {
                                                            "data": {
                                                                "success": "0.0%",  # noqa
                                                                "min": 0.113,
                                                                "max": 0.113,
                                                                "median": 0.113,  # noqa
                                                                "95%ile": 0.113,  # noqa
                                                                "iteration_count": 1,  # noqa
                                                                "avg": 0.113,
                                                                "90%ile": 0.113
                                                            },
                                                            "display_name": "glance_v2.get_image",  # noqa
                                                            "name": "glance_v2.get_image",  # noqa
                                                            "count_per_iteration": 1,  # noqa
                                                            "children": []
                                                        }
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                }
                            },
                            "data": [
                                {
                                    "timestamp": 1586807411.442345,
                                    "error": [],
                                    "duration": 58.35123419761658,
                                    "output": {
                                        "additive": [],
                                        "complete": []
                                    },
                                    "idle_duration": 20.0,
                                    "atomic_actions": [
                                        {
                                            "finished_at": 1586807436.653431,
                                            "started_at": 1586807411.442428,
                                            "name": "nova.boot_server",
                                            "children": []
                                        },
                                        {
                                            "finished_at": 1586807448.355225,
                                            "started_at": 1586807436.653565,
                                            "name": "nova.snapshot_server",
                                            "children": [
                                                {
                                                    "finished_at": 1586807437.776488,  # noqa
                                                    "started_at": 1586807437.563398,  # noqa
                                                    "name": "glance_v2.get_image",  # noqa
                                                    "children": []
                                                },
                                                {
                                                    "finished_at": 1586807448.355212,  # noqa
                                                    "started_at": 1586807437.776796,  # noqa
                                                    "name": "glance.wait_for_image",  # noqa
                                                    "children": [
                                                        {
                                                            "finished_at": 1586807437.903536,  # noqa
                                                            "started_at": 1586807437.776933,  # noqa
                                                            "name": "glance_v2.get_image",  # noqa
                                                            "children": []
                                                        },
                                                        {
                                                            "finished_at": 1586807440.044981,  # noqa
                                                            "started_at": 1586807439.90577,  # noqa
                                                            "name": "glance_v2.get_image",  # noqa
                                                            "children": []
                                                        },
                                                        {
                                                            "finished_at": 1586807442.171703,  # noqa
                                                            "started_at": 1586807442.046665,  # noqa
                                                            "name": "glance_v2.get_image",  # noqa
                                                            "children": []
                                                        },
                                                        {
                                                            "finished_at": 1586807444.290354,  # noqa
                                                            "started_at": 1586807444.172236,  # noqa
                                                            "name": "glance_v2.get_image",  # noqa
                                                            "children": []
                                                        },
                                                        {
                                                            "finished_at": 1586807446.32075,  # noqa
                                                            "started_at": 1586807446.291259,  # noqa
                                                            "name": "glance_v2.get_image",  # noqa
                                                            "children": []
                                                        },
                                                        {
                                                            "finished_at": 1586807448.355074,  # noqa
                                                            "started_at": 1586807448.321069,  # noqa
                                                            "name": "glance_v2.get_image",  # noqa
                                                            "children": []
                                                        }
                                                    ]
                                                }
                                            ]
                                        },
                                        {
                                            "finished_at": 1586807457.173632,
                                            "started_at": 1586807448.355274,
                                            "name": "nova.delete_server",
                                            "children": []
                                        },
                                        {
                                            "finished_at": 1586807481.154014,
                                            "started_at": 1586807457.173697,
                                            "name": "nova.boot_server",
                                            "children": []
                                        },
                                        {
                                            "finished_at": 1586807487.938179,
                                            "started_at": 1586807481.154141,
                                            "name": "nova.delete_server",
                                            "children": []
                                        },
                                        {
                                            "finished_at": 1586807489.793556,
                                            "started_at": 1586807487.938207,
                                            "name": "nova.delete_image",
                                            "children": [
                                                {
                                                    "finished_at": 1586807489.679619,  # noqa
                                                    "started_at": 1586807487.939451,  # noqa
                                                    "name": "glance_v2.delete_image",  # noqa
                                                    "children": []
                                                },
                                                {
                                                    "finished_at": 1586807489.79353,  # noqa
                                                    "started_at": 1586807489.679957,  # noqa
                                                    "name": "glance.wait_for_delete",  # noqa
                                                    "children": [
                                                        {
                                                            "finished_at": 1586807489.79346,  # noqa
                                                            "started_at": 1586807489.680148,  # noqa
                                                            "children": [],
                                                            "failed": True,
                                                            "name": "glance_v2.get_image"  # noqa
                                                        }
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ],
                            "failed_iteration_count": 0,
                            "total_iteration_count": 1,
                            "created_at": "2020-04-13T19:50:10",
                            "updated_at": "2020-04-13T19:51:33",
                            "contexts": {},
                            "contexts_results": [
                                {
                                    "setup": {
                                        "finished_at": 1586807410.45964,
                                        "started_at": 1586807410.372313,
                                        "atomic_actions": [],
                                        "error": None
                                    },
                                    "cleanup": {
                                        "finished_at": 1586807490.967003,
                                        "started_at": 1586807490.9666,
                                        "atomic_actions": [],
                                        "error": None
                                    },
                                    "plugin_cfg": {
                                        "user_choice_method": "random"
                                    },
                                    "plugin_name": "users@openstack"
                                },
                                {
                                    "setup": {
                                        "finished_at": 1586807410.460624,
                                        "started_at": 1586807410.460621,
                                        "atomic_actions": [],
                                        "error": None
                                    },
                                    "cleanup": {
                                        "finished_at": 1586807490.965353,
                                        "started_at": 1586807489.816932,
                                        "atomic_actions": [],
                                        "error": None
                                    },
                                    "plugin_cfg": [
                                        "nova",
                                        "glance"
                                    ],
                                    "plugin_name": "cleanup@openstack"
                                }
                            ],
                            "position": 0,
                            "pass_sla": True,
                            "sla_results": {
                                "sla": [
                                    {
                                        "criterion": "failure_rate",
                                        "detail": "Failure rate criteria 0.00% <= 0.00% <= 0.00% - Passed",  # noqa
                                        "success": True
                                    }
                                ]
                            },
                            "sla": {
                                "failure_rate": {
                                    "max": 0
                                }
                            }
                        }
                    ]
                }
            ]
        }
    ]
}

MEASURES_SNAPSHOT_SERVER = [
    {
        "measurement": "rally_task",
        "tags": {
            "deployment": "crit_project_001",
            "component": "glance",
            'host': 'fake',
            "scenario": "snapshot_server",
            "task": "total"
        },
        "time": "2020-04-13T19:50:08",
        "fields": {
            "duration": 78.351,
            "failure": 0
        }
    },
    {
        "measurement": "rally_task",
        "tags": {
            "deployment": "crit_project_001",
            "component": "glance",
            'host': 'fake',
            "scenario": "snapshot_server",
            "task": "nova.boot_server"
        },
        "time": "2020-04-13T19:50:08",
        "fields": {
            "duration": 49.191,
            "failure": 0
        }
    },
    {
        "measurement": "rally_task",
        "tags": {
            "deployment": "crit_project_001",
            "component": "glance",
            'host': 'fake',
            "scenario": "snapshot_server",
            "task": "nova.snapshot_server"
        },
        "time": "2020-04-13T19:50:08",
        "fields": {
            "duration": 11.702,
            "failure": 0
        }
    },
    {
        "measurement": "rally_task",
        "tags": {
            "deployment": "crit_project_001",
            "component": "glance",
            'host': 'fake',
            "scenario": "snapshot_server",
            "task": "nova.delete_server"
        },
        "time": "2020-04-13T19:50:08",
        "fields": {
            "duration": 15.602,
            "failure": 0
        }
    },
    {
        "measurement": "rally_task",
        "tags": {
            "deployment": "crit_project_001",
            "component": "glance",
            'host': 'fake',
            "scenario": "snapshot_server",
            "task": "nova.delete_image"
        },
        "time": "2020-04-13T19:50:08",
        "fields": {
            "duration": 1.855,
            "failure": 0
        }
    }
]

REPORT_FAILURE_BOOT = {
    "info": {
        "generated_at": "2020-04-15T14:52:07",
        "rally_version": "2.0.0",
        "format_version": "1.2"
    },
    "tasks": [
        {
            "uuid": "2d57ebb1-8b40-4552-b54f-23aa6f0690e1",
            "title": "",
            "description": "",
            "status": "finished",
            "tags": [],
            "env_uuid": "548cd2be-6f9b-45cb-b09c-3cb2b09f9b64",
            "env_name": "gva_project_013",
            "created_at": "2020-04-15T14:18:06",
            "updated_at": "2020-04-15T14:24:10",
            "pass_sla": False,
            "subtasks": [
                {
                    "uuid": "4317b774-c52d-4c75-b79c-a39cabf8e3f7",
                    "title": "NovaServers.boot_and_delete_server",
                    "description": "",
                    "status": "finished",
                    "created_at": "2020-04-15T14:18:09",
                    "updated_at": "2020-04-15T14:24:10",
                    "sla": {},
                    "workloads": [
                        {
                            "uuid": "a9b00308-08cd-4776-ae7f-02dee8b73514",
                            "description": "Boot and delete a server.",
                            "runner": {
                                "constant": {
                                    "concurrency": 1,
                                    "times": 1
                                }
                            },
                            "hooks": [],
                            "scenario": {
                                "NovaServers.boot_and_delete_server": {
                                    "force_delete": False,
                                    "flavor": {
                                        "name": "rally.nano"
                                    },
                                    "image": {
                                        "name": "Rally CirrOS IPv6"
                                    }
                                }
                            },
                            "min_duration": 345.613,
                            "max_duration": 345.613,
                            "start_time": 1586960289.64881,
                            "load_duration": 345.613,
                            "full_duration": 359.843,
                            "statistics": {
                                "durations": {
                                    "total": {
                                        "data": {
                                            "success": "0.0%",
                                            "min": 355.613,
                                            "max": 355.613,
                                            "median": 355.613,
                                            "95%ile": 355.613,
                                            "iteration_count": 1,
                                            "avg": 355.613,
                                            "90%ile": 355.613
                                        },
                                        "display_name": "total",
                                        "name": "total",
                                        "count_per_iteration": 1,
                                        "children": [
                                            {
                                                "data": {
                                                    "success": "0.0%",
                                                    "min": 345.613,
                                                    "max": 345.613,
                                                    "median": 345.613,
                                                    "95%ile": 345.613,
                                                    "iteration_count": 1,
                                                    "avg": 345.613,
                                                    "90%ile": 345.613
                                                },
                                                "display_name": "duration",
                                                "name": "duration",
                                                "count_per_iteration": 1,
                                                "children": []
                                            },
                                            {
                                                "data": {
                                                    "success": "0.0%",
                                                    "min": 10.0,
                                                    "max": 10.0,
                                                    "median": 10.0,
                                                    "95%ile": 10.0,
                                                    "iteration_count": 1,
                                                    "avg": 10.0,
                                                    "90%ile": 10.0
                                                },
                                                "display_name": "idle_duration",  # noqa
                                                "name": "idle_duration",
                                                "count_per_iteration": 1,
                                                "children": []
                                            }
                                        ]
                                    },
                                    "atomics": [
                                        {
                                            "data": {
                                                "success": "0.0%",
                                                "min": 355.613,
                                                "max": 355.613,
                                                "median": 355.613,
                                                "95%ile": 355.613,
                                                "iteration_count": 1,
                                                "avg": 355.613,
                                                "90%ile": 355.613
                                            },
                                            "display_name": "nova.boot_server",
                                            "name": "nova.boot_server",
                                            "count_per_iteration": 1,
                                            "children": []
                                        }
                                    ]
                                }
                            },
                            "data": [
                                {
                                    "timestamp": 1586960289.648806,
                                    "error": [
                                        "GetResourceErrorStatus",
                                        "Resource <Server: rally-a9b0-k2sk> has ERROR status.",  # noqa
                                        " Fault: {u'message': u'internal error: qemu unexpectedly closed the monitor: ioctl(KVM_CREATE_VM) failed: 12 Cannot allocate memory",  # noqa
                                        "2020-04-15T14:23:55.042900Z qemu-kvm: failed to initialize KVM: Cannot allocate memory',u'code': 500,u'created': u'2020-04-15T14:24:05Z'}",  # noqa
                                        "Traceback (most recent call last):",
                                        "  File \"/usr/lib/python2.7/site-packages/rally/task/runner.py\",line 71,in _run_scenario_once",  # noqa
                                        "    getattr(scenario_inst,method_name)(**scenario_kwargs)",  # noqa
                                        "  File \"/usr/lib/python2.7/site-packages/rally_openstack/scenarios/nova/servers.py\",line 118,in run",  # noqa
                                        "    server = self._boot_server(image,flavor,**kwargs)",  # noqa
                                        "  File \"/usr/lib/python2.7/site-packages/rally/task/atomic.py\",line 91,in func_atomic_actions",  # noqa
                                        "    f = func(self,*args,**kwargs)",
                                        "  File \"/usr/lib/python2.7/site-packages/rally_openstack/scenarios/nova/utils.py\",line 88,in _boot_server",  # noqa
                                        "    check_interval=CONF.openstack.nova_server_boot_poll_interval\"",  # noqa
                                        "  File \"/usr/lib/python2.7/site-packages/rally/task/utils.py\",line 214,in wait_for_status",  # noqa
                                        "    resource = update_resource(resource)",  # noqa
                                        "  File \"/usr/lib/python2.7/site-packages/rally/task/utils.py\",line 89,in _get_from_manager",  # noqa
                                        "    fault=getattr(res,\"fault\",\"n/a\"))",  # noqa
                                        "GetResourceErrorStatus: Resource <Server: rally-a9b0-k2sk> has ERROR status.",  # noqa
                                        " Fault: {u'message': u'internal error: qemu unexpectedly closed the monitor: ioctl(KVM_CREATE_VM) failed: 12 Cannot allocate memory",  # noqa
                                        "2020-04-15T14:23:55.042900Z qemu-kvm: failed to initialize KVM: Cannot allocate memory',u'code': 500,u'created': u'2020-04-15T14:24:05Z'}"  # noqa
                                    ],
                                    "duration": 345.6128590106964,
                                    "output": {
                                        "additive": [],
                                        "complete": []
                                    },
                                    "idle_duration": 10.0,
                                    "atomic_actions": [
                                        {
                                            "finished_at": 1586960645.261578,
                                            "started_at": 1586960289.648931,
                                            "children": [],
                                            "failed": True,
                                            "name": "nova.boot_server"
                                        }
                                    ]
                                }
                            ],
                            "failed_iteration_count": 1,
                            "total_iteration_count": 1,
                            "created_at": "2020-04-15T14:18:09",
                            "updated_at": "2020-04-15T14:24:10",
                            "contexts": {},
                            "contexts_results": [
                                {
                                    "setup": {
                                        "finished_at": 1586960288.822856,
                                        "started_at": 1586960288.719112,
                                        "atomic_actions": [],
                                        "error": None
                                    },
                                    "cleanup": {
                                        "finished_at": 1586960648.558229,
                                        "started_at": 1586960648.557742,
                                        "atomic_actions": [],
                                        "error": None
                                    },
                                    "plugin_cfg": {
                                        "user_choice_method": "random"
                                    },
                                    "plugin_name": "users@openstack"
                                },
                                {
                                    "setup": {
                                        "finished_at": 1586960288.824143,
                                        "started_at": 1586960288.824139,
                                        "atomic_actions": [],
                                        "error": None
                                    },
                                    "cleanup": {
                                        "finished_at": 1586960648.556936,
                                        "started_at": 1586960645.315186,
                                        "atomic_actions": [],
                                        "error": None
                                    },
                                    "plugin_cfg": [
                                        "nova"
                                    ],
                                    "plugin_name": "cleanup@openstack"
                                }
                            ],
                            "position": 0,
                            "pass_sla": False,
                            "sla_results": {
                                "sla": [
                                    {
                                        "criterion": "failure_rate",
                                        "detail": "Failure rate criteria 0.00% <= 100.00% <= 0.00% - Failed",  # noqa
                                        "success": False
                                    }
                                ]
                            },
                            "sla": {
                                "failure_rate": {
                                    "max": 0
                                }
                            }
                        }
                    ]
                }
            ]
        }
    ]
}

MEASURES_FAILURE_BOOT = [
    {
        "measurement": "rally_task",
        "tags": {
            "deployment": "gva_project_013",
            "component": "nova",
            'host': 'fake',
            "scenario": "boot_server",
            "task": "total"
        },
        "time": "2020-04-15T14:18:06",
        "fields": {
            "duration": 355.613,
            "failure": 1
        }
    },
    {
        "measurement": "rally_task",
        "tags": {
            "deployment": "gva_project_013",
            "component": "nova",
            'host': 'fake',
            "scenario": "boot_server",
            "task": "nova.boot_server"
        },
        "time": "2020-04-15T14:18:06",
        "fields": {
            "duration": 355.613,
            "failure": 1
        }
    }
]

REPORT_CRASHED_IRONIC = {
    "info": {
        "generated_at": "2020-04-24T13:08:43",
        "rally_version": "2.0.0",
        "format_version": "1.2"
    },
    "tasks": [
        {
            "uuid": "3a6d00ca-af72-4405-89e6-85ae06bcd43c",
            "title": "",
            "description": "",
            "status": "crashed",
            "tags": [],
            "env_uuid": "a1d5ab54-45f9-48fa-a5d2-22e449a85e43",
            "env_name": "teststack_cell03",
            "created_at": "2020-04-24T13:05:05",
            "updated_at": "2020-04-24T13:05:05",
            "pass_sla": True,
            "subtasks": []
        }
    ]
}

MEASURES_CRASHED_IRONIC = [
    {
        'fields': {
            'duration': 0,
            'failure': 1
        },
        'measurement': 'rally_task',
        'tags': {
            'component': 'ironic',
            'deployment': 'teststack_cell03',
            'host': 'fake',
            'scenario': 'boot-baremetal',
            'task': 'total'
        },
        'time': '2020-04-24T13:05:05'
    }
]

REPORT_CRASHED_GLANCE = {
    'info': {
        'rally_version': '2.0.0',
        'generated_at': '2020-05-19T13:23:16',
        'format_version': '1.2'
    },
    'tasks': [
        {
            'uuid': 'd89cb68c-a968-452d-ab73-390f7218276e',
            'title': '',
            'description': '',
            'status': 'finished',
            'tags': [],
            'env_uuid': 'aa985ebe-25b5-4d27-b464-a47362ae2a4e',
            'env_name': 'teststack_cell01',
            'created_at': '2020-05-19T13:23:10',
            'updated_at': '2020-05-19T13:23:14',
            'pass_sla': False,
            'subtasks': [
                {
                    'uuid': '78069c3a-605b-48e5-8bab-283b662d76e8',
                    'title': 'GlanceImages.create_and_delete_image',
                    'description': '',
                    'status': 'finished',
                    'created_at': '2020-05-19T13:23:11',
                    'updated_at': '2020-05-19T13:23:13',
                    'sla': {},
                    'workloads': [
                        {
                            'uuid': '9f07b875-a740-4999-987f-d9d6551e5748',
                            'description': 'Create and then delete an image.',
                            'runner': {
                                'constant': {
                                    'times': 1,
                                    'concurrency': 1
                                }
                            },
                            'hooks': [],
                            'scenario': {
                                'GlanceImages.create_and_delete_image': {
                                    'image_location': 'http://download.cirros-cloud.net/0.3.0/cirros-0.3.0-i386-disk.img',  # noqa
                                    'container_format': 'bare',
                                    'disk_format': 'qcow2'
                                }
                            },
                            'min_duration': None,
                            'max_duration': None,
                            'start_time': None,
                            'load_duration': 0.0,
                            'full_duration': 0.719708,
                            'statistics': {
                                'durations': {
                                    'total': {
                                        'data': {
                                            'iteration_count': 0,
                                            'min': 'n/a',
                                            'median': 'n/a',
                                            '90%ile': 'n/a',
                                            '95%ile': 'n/a',
                                            'max': 'n/a',
                                            'avg': 'n/a',
                                            'success': 'n/a'
                                        },
                                        'count_per_iteration': 1,
                                        'name': 'total',
                                        'display_name': 'total',
                                        'children': []
                                    },
                                    'atomics': []
                                }
                            },
                            'data': [],
                            'failed_iteration_count': 0,
                            'total_iteration_count': 0,
                            'created_at': '2020-05-19T13:23:11',
                            'updated_at': '2020-05-19T13:23:13',
                            'contexts': {},
                            'contexts_results': [
                                {
                                    'plugin_name': 'users@openstack',
                                    'plugin_cfg': {
                                        'user_choice_method': 'random'
                                    },
                                    'setup': {
                                        'started_at': 1589894591.4519546,
                                        'finished_at': 1589894591.5421205,
                                        'atomic_actions': [],
                                        'error': None
                                    },
                                    'cleanup': {
                                        'started_at': 1589894592.1706736,
                                        'finished_at': 1589894592.1709228,
                                        'atomic_actions': [],
                                        'error': None
                                    }
                                }, {
                                    'plugin_name': 'cleanup@openstack',
                                    'plugin_cfg': ['glance'],
                                    'setup': {
                                        'started_at': 1589894591.5429714,
                                        'finished_at': 1589894591.5429742,
                                        'atomic_actions': [],
                                        'error': None
                                    },
                                    'cleanup': {
                                        'started_at': 1589894591.8370957,
                                        'finished_at': 1589894592.170111,
                                        'atomic_actions': [],
                                        'error': None
                                    }
                                }
                            ],
                            'position': 0,
                            'pass_sla': False,
                            'sla_results': {
                                'sla': [
                                    {
                                        'criterion': 'failure_rate',
                                        'success': True,
                                        'detail': 'Failure rate criteria 0.00% <= 0.00% <= 0.00% - Passed'  # noqa
                                    }, {
                                        'criterion': 'something_went_wrong',
                                        'success': False,
                                        'detail': "Unexpected error: Invalid scenario argument: 'Url error http://download.cirros-cloud.net/0.3.0/cirros-0.3.0-i386-disk.img (Invalid scenario argument: 'Url http://download.cirros-cloud.net/0.3.0/cirros-0.3.0-i386-disk.img unavailable (code 302)')'"  # noqa
                                    }
                                ]
                            },
                            'sla': {
                                'failure_rate': {
                                    'max': 0
                                }
                            }
                        }
                    ]
                }
            ]
        }
    ]
}

MEASURES_CRASHED_GLANCE = [
    {
        'fields': {
            'duration': 0,
            'failure': 1
        },
        'measurement': 'rally_task',
        'tags': {
            'component': 'glance',
            'deployment': 'teststack_cell01',
            'host': 'fake',
            'scenario': 'create-and-delete-image',
            'task': 'total'
        },
        'time': '2020-05-19T13:23:10'
    }
]
