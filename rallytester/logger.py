def logging_config(level, file_, stdout, short_task, deployment, component):
    """Configure appropriate logger.

    :param level: Level for the logger
    :param file_: Destination of the application logs.
    :param stdout: Boolean value that indicated if the
        logger is set to redirect the output to stdout or not.
        If this is true, it will ignore the `file_` parameter
    :param short_task: The name of the task that is being executed (references
        the json where it has the configuration)
    :param deployment: The deployment where this test is being executed
    :returns: A dict with a correct configuration to be used
        by logging.config.dictConfig in the main section
    """
    if stdout:
        handler = {
            'default': {
                'class': 'logging.StreamHandler',
                'formatter': 'standard',
                'stream': 'ext://sys.stdout'
            }
        }
    else:
        handler = {
            'default': {
                'class': 'logging.FileHandler',
                'filename': file_,
                'formatter': 'standard'
            }
        }

    return {
        'version': 1,
        'disable_existing_loggers': False,
        'handlers': handler,
        'formatters': {
            'standard': {
                'format': '%(asctime)s.%(msecs)03d %(process)d %(levelname)s %(name)s [-] [{component} {task} {deployment}] %(message)s'.format(task=short_task, deployment=deployment, component=component),  # noqa
                'datefmt': '%Y-%m-%d %H:%M:%S'
            }
        },
        'loggers': {
            '': {
                'handlers': ['default'],
                'level': level
            }
        }
    }
