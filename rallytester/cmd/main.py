#!/usr/bin/python3
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import argparse
import logging
import logging.config
import sys
import yaml
import yaml.parser

from rallytester import conf
from rallytester.engine import RallyEngine
from rallytester import logger
from rallytester import notifier

log = logging.getLogger(__name__)

CONF = conf.CONF
CONF(args=[], project='rallytester')


def YAMLParse(string):
    value = {}
    try:
        data = yaml.safe_load(string)
        if isinstance(data, str):
            raise yaml.parser.ParserError("String '%s' doesn't look like a "
                                          "dictionary." % string)
        value.update(data)
    except yaml.parser.ParserError:
        args = [keypair.split("=", 1)
                for keypair in string.split(",")]
        if len([a for a in args if len(a) != 1]) != len(args):
            raise yaml.parser.ParserError("Value has to be YAML or JSON")
        else:
            value.update(dict(args))
    return value


class RallyTesterCMD(object):
    def __init__(self):
        self.parser = argparse.ArgumentParser(
            description="Command line interface to perform tests over our "
                        "production cloud")
        self.parser.add_argument("--task", required=True)
        self.parser.add_argument("--deployment", required=True)
        self.parser.add_argument(
            "--task-args", dest="task_args",
            help="Input task args (JSON or YAML dict)",
            type=YAMLParse,
            default={})
        self.parser.add_argument(
            "--debug", action='store_true', default=False)

    def parse_args(self, args):
        return self.parser.parse_args(args)

    def configure_logging(self, args, short_task, component):
        # Configure the logger to have the correct format
        logging.config.dictConfig(
            logger.logging_config(
                CONF.logging.log_level,
                CONF.logging.log_file,
                args.debug,
                short_task,
                args.deployment,
                component
            )
        )

    def main(self, args):
        args = self.parse_args(args)

        # from /opt/rally-scenarios/glance/list-images.json -> list-images
        short_task = args.task.split('/')[-1].replace('.json', '')
        component = args.task.split('/')[-2].replace('.json', '')

        # Remove architecture
        for arch in ['_x86_64', '_aarch64']:
            short_task = short_task.replace(arch, '')
            component = component.replace(arch, '')

        self.configure_logging(args, short_task, component)

        log = logging.getLogger('rallytester')
        log.debug('Start: Rally tester')
        log.debug('Arguments: %s', args)

        # Start rally tester
        log.info('Start: Rally test against %s using %s',
                 args.deployment,
                 args.task)

        # We execute the rally task
        result, error_msg = RallyEngine().execute_task(
            args.deployment,
            args.task,
            args.task_args)
        notifier.print_result(result)

        if error_msg:
            # If it failed for any reason, we send an email
            log.info('Failure in test, sending notification to %s',
                     CONF.alert.alert_to)
            notifier.send_alarm(args.task, args.deployment, error_msg,
                                CONF.alert.alert_to)

        log.info('Finish: Rally test against %s using %s',
                 args.deployment,
                 args.task)


# Needs static method for setup.cfg
def main(args=None):
    try:
        RallyTesterCMD().main(args)
    except Exception as e:
        log.exception(e)
        sys.exit(-1)


if __name__ == "__main__":
    main()
