from oslo_config import cfg


log_level = cfg.StrOpt(
    'log_level',
    default='INFO',
    help="Log level. Defaults to INFO")

log_file = cfg.StrOpt(
    'log_file',
    default='/var/log/rally-tester/rally-tester.log',
    help="Log file where to store execution logs")

GROUP_NAME = __name__.split('.')[-1]
ALL_OPTS = [
    log_level,
    log_file
]


def register_opts(conf):
    conf.register_opts(ALL_OPTS, group=GROUP_NAME)


def list_opts():
    return {GROUP_NAME: ALL_OPTS}
