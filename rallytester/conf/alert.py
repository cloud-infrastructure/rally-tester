from oslo_config import cfg


alert_to = cfg.StrOpt(
    'alert_to',
    default=None,
    help="Notify via mail in case of errors")

GROUP_NAME = __name__.split('.')[-1]
ALL_OPTS = [
    alert_to
]


def register_opts(conf):
    conf.register_opts(ALL_OPTS, group=GROUP_NAME)


def list_opts():
    return {GROUP_NAME: ALL_OPTS}
