from oslo_config import cfg
from stevedore.extension import ExtensionManager

enabled_backends = cfg.ListOpt(
    'enabled_backends',
    default='influx',
    help='A list of backend names to use. These backend names '
         'should be backed by a unique [CONFIG] group '
         'with its options')

yaql_expression = cfg.StrOpt(
    'yaql_expression',
    default='''
let(
  deployment => $$.tasks[0].env_name,
  component => env().component,
  scenario => env().scenario,
  host => env().host,
  extra_tags => env().extra_tags,
  time => $$.tasks[0].created_at,
  data => selectCase(
      ($$.tasks[0].subtasks and $$.tasks[0].subtasks[0].workloads[0].statistics.durations['total'].data),
      ($$.tasks[0].subtasks and list($$.tasks[0].subtasks[0].workloads[0].contexts_results.where($$.setup.error))),
      ($$.tasks[0].status = 'crashed' or (not $$.tasks[0].pass_sla and not $$.tasks[0].subtasks[0].workloads[0].data))
  ).switchCase(
    $$.tasks[0].subtasks[0].workloads[0].statistics.durations['total'].data,
    null,
    null
  )
) -> selectCase(not $$data).switchCase(
  list(
    dict(
      measurement => 'rally_task',
      tags => dict(
        deployment => $$deployment,
        component => $$component,
        scenario => $$scenario,
        host => $$host,
        task => "total"
      ) + $$extra_tags,
      time => $$time,
      fields => dict(
        duration => 0.0,
        failure => 1
      )
    )
  ), (
    list(
      dict(
        measurement => 'rally_task',
        tags => dict(
          deployment => $$deployment,
          component => $$component,
          scenario => $$scenario,
          host => $$host,
          task => "total"
        ) + $$extra_tags,
        time => $$time,
        fields => dict(
            duration => switch(
              $$data.max = "n/a" => 0.0,
              $$data.max != "n/a" => $$data.max
            ),
            failure => switch(
                $$data.success = "100.0%" => 0,
                $$data.success != "100.0%" => 1
            )
        )
      )
    ) + $$.tasks[0].subtasks[0].workloads[0].statistics.durations.atomics.selectMany(
      dict(
        measurement => 'rally_task',
        tags => dict(
          deployment => $$deployment,
          component => $$component,
          scenario => $$scenario,
          host => $$host,
          task => $$.name
        ) + $$extra_tags,
        time => $$time,
        fields => dict(
          duration => $$.data.max,
          failure => switch(
            $$.data.success = "100.0%" => 0,
            $$.data.success != "100.0%" => 1
          )
        )
      )
    )
  )
)''',  # noqa
    help="YAQL expression to fetch information from report")

GROUP_NAME = __name__.split('.')[-1]
ALL_OPTS = [
    enabled_backends,
    yaql_expression,
]


def register_opts(conf):
    conf.register_opts(ALL_OPTS, group=GROUP_NAME)


def list_opts():
    opts = {GROUP_NAME: ALL_OPTS}

    for driver in ExtensionManager('rallytester.reporter.backends'):
        opts[driver.name] = driver.plugin.get_driver_options()

    return opts
