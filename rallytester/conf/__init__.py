from oslo_config import cfg

from rallytester.conf import alert
from rallytester.conf import logging
from rallytester.conf import regex
from rallytester.conf import reporter

conf_modules = [
    alert,
    logging,
    regex,
    reporter,
]

CONF = cfg.CONF

for module in conf_modules:
    module.register_opts(CONF)
