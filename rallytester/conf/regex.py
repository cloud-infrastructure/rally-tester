from oslo_config import cfg

enabled_regex = cfg.ListOpt(
    'enabled_regex',
    default=[
        'raw_substring',
        'date',
        'datetime',
        'ip6',
        'cluster',
        'pod',
        'httpheaderdict',
        'json_message',
        'http_replace',
        'ip_replace',
        'id_rally_replace',
        'req_replace',
        'long_id_replace',
        'migration',
        'resource_replace',
        'quota_exceeded',
        'rally_tired',
        'remote_error',
        'resource_limit',
        'invalid_flavor',
        'traceback'
    ],
    help='Ordered list of enabled regular expressions to be used while '
         'trimming the error message.')

regex_dict = cfg.DictOpt(
    'regex_dict',
    default={
        "raw_substring": ".* (ERROR rallytester\\.rallytester)\\s\\[[^\\]]"
                         "*\\]\\s\\[[^\\]]*\\]\\s(Task failed:\\s*|)(.*)",
        "date": "[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}T[0-9]{1,2}:[0-9]{2}:[0-9]{2}("
                ".[0-9]{1,6})?Z",
        "datetime": "datetime.datetime\\([0-9]{4}, [0-9]{1,2}, [0-9]{1,2}, "
                    "[0-9]{1,2}, [0-9]{1,2}, [0-9]{1,2}, tzinfo=tzutc\\(\\)"
                    "\\)",
        "ip6": "'?([a-f0-9:]+:+)+[a-f0-9]+'?",
        "cluster": "([^<]*<\\S*)\\s*\\{[^>]*(>.*)",
        "pod": "pods\\/\\S+",
        "httpheaderdict": "(.*)HTTPHeaderDict\\(\\{.*",
        "json_message": "Resource (.*)\\'message\\': u(\\'|\")(.*)(\\'|\"), "
                        "u\\'code\\'(.*)",
        "http_replace": "(http[s]?\\:\\/\\/)([^\\/]*)/\\S*",
        "ip_replace": "[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}",
        "id_rally_replace": "rally\\-([\\w\\-]{8,})",
        "req_replace": "req-[\\w\\-\\:]{12,}",
        "long_id_replace": "([a-f0-9]{32}|[a-f0-9\\-]{36})",
        "migration": "(.*)(Migration failed)(.*)(change host)(.*)",
        "resource_replace": "Resource [a-z0-9\\-]{15,}",
        "resource_limit": "(.*)(Resource limit exceeded: .* per project)(.*)",
        "quota_exceeded": "Requested [0-9]*, [0-9]*, but already used [0-9]*,"
                          " [0-9]* of [0-9]*, [0-9]*",
        "rally_tired": "(Rally tired waiting)\\s([0-9]*\\.00 seconds for|for)"
                       "\\s(\\S*)\\s\\S* to become (\\([^\\)]*\\)|([^\\(]\\S*"
                       ")) .*",
        "remote_error": "(Remote error: )(\\S*\\s).*",
        "invalid_flavor": "^(\\S* plugin) (\\S*) doesn't pass (\\S*) validati"
                          "on. Details: .*",
        "traceback": "(\\n)?Traceback \\(most recent call last\\)[\\s\\S]*"
    },
    help='Regular expressions available for trimming.'
)

substitution_dict = cfg.DictOpt(
    'substitution_dict',
    default={
        "raw_substring": "\\3",
        "cluster": "\\1\\2",
        "httpheaderdict": "\\1HTTPHeaderDict",
        "json_message": "\\3",
        "http_replace": "\\1\\2/...",
        "ip_replace": "xyz.xyz.xyz.xyz",
        "id_rally_replace": "rally-xyz",
        "req_replace": "req-xyz",
        "long_id_replace": "xyz",
        "migration": "migration failed",
        "resource_replace": "Resource xyz",
        "resource_limit": "\\2",
        "quota_exceeded": "Requested .....",
        "rally_tired": "waiting for \\3 to become \\4",
        "remote_error": "\\1 \\2",
        "invalid_flavor": "Task \\2 fail \\3",
        "date": "date",
        "datetime": "datetime",
        "ip6": "wxyz:wxyz:wxyz::wxyz",
        "pod": "pods/podXYZ",
        "traceback": ""
    },
    help='Substitutions that can be executed if there is a match on '
         'regular expressions.'
)

max_string_chars = cfg.IntOpt(
    'max_string_chars',
    default=300,
    help='Maximum length of the trimmed string'
)

GROUP_NAME = __name__.split('.')[-1]
ALL_OPTS = [
    enabled_regex,
    regex_dict,
    substitution_dict,
    max_string_chars,
]


def register_opts(conf):
    conf.register_opts(ALL_OPTS, group=GROUP_NAME)


def list_opts():
    return {GROUP_NAME: ALL_OPTS}
