import logging


from influxdb import exceptions
from influxdb import InfluxDBClient
from oslo_config import cfg
from rallytester.reporter.backend import Backend

log = logging.getLogger(__name__)

influx_opts = [
    cfg.StrOpt(
        'host',
        default='dbod-ccinflux.cern.ch',
        help="Host to use for influxdb"),
    cfg.IntOpt(
        'port',
        default=8087,
        help="Port to use for influxdb"),
    cfg.StrOpt(
        'database',
        default='dblogger',
        help="Database for influxdb"),
    cfg.StrOpt(
        'user',
        default='fake_user',
        help="Defalt user for influxdb"),
    cfg.StrOpt(
        'password',
        default='fake_password',
        help="Defalt password for influxdb"),
    cfg.BoolOpt(
        'ssl',
        default=True,
        help="Use ssl to connect to the database")
]

CONF = cfg.CONF
CONF.register_opts(influx_opts, group='influx')


class InfluxBackend(Backend):
    def __init__(self, *args, **kwargs):
        super(InfluxBackend, self).__init__(*args, **kwargs)
        self.configuration.append_config_values(influx_opts)

    def send_measurements(self, measurements):
        client = InfluxDBClient(
            host=self.configuration.host,
            port=self.configuration.port,
            username=self.configuration.user,
            password=self.configuration.password,
            database=self.configuration.database,
            ssl=self.configuration.ssl
        )

        try:
            if client.write_points(measurements):
                log.debug("Measurements sent successfully")
            else:
                log.error(
                    "Error while storing measurements %s",
                    measurements
                )
        except exceptions.InfluxDBClientError:
            log.exception("Exception while storing measurements")

    @classmethod
    def get_driver_options(cls):
        return influx_opts + Backend.get_driver_options()
