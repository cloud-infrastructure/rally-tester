
import logging

from rallytester.reporter.backend import Backend

log = logging.getLogger(__name__)


class LogBackend(Backend):

    def send_measurements(self, measurements):
        for measurement in measurements:
            log.info("Store %s", measurement)
