import abc
import logging

from oslo_config import cfg

log = logging.getLogger(__name__)

backend_opts = [
    cfg.ListOpt(
        'allowlist',
        default=[],
        help='List of measurements allowed to be stored in the backend'),
    cfg.ListOpt(
        'denylist',
        default=[],
        help='List of measurements not allowed to be stored in the backend')
]


class Backend(object):
    """Base class for reporter backends."""

    def __init__(self, *args, **kwargs):
        super(Backend, self).__init__()

        self.configuration = kwargs.get('configuration', None)

        if self.configuration:
            self.configuration.append_config_values(backend_opts)

        self.allowlist = (
            self.configuration
            and self.configuration.safe_get('allowlist')
        )

        self.denylist = (
            self.configuration
            and self.configuration.safe_get('denylist')
        )

    def is_allowed(self, measurement):
        if self.denylist and measurement in self.denylist:
            return False
        elif self.allowlist and measurement not in self.allowlist:
            return False
        else:
            return True

    def report_measurements(self, measurements):
        self.send_measurements([
            m for m in measurements if self.is_allowed(m)
        ])

    @abc.abstractmethod
    def send_measurements(self, measurements):
        pass

    @classmethod
    def get_driver_options(cls):
        """Return the oslo_config options specific to the driver."""
        return backend_opts
