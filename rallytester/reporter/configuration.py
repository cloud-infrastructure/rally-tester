"""
Configuration support for all drivers.

This module allows support for setting configurations either from default
or from a particular CONF group, to be able to set multiple configurations
for a given set of values.

For instance, two generic configurations can be set by naming them in groups as

 [generic1]
 share_backend_name=generic-backend-1
 ...

 [generic2]
 share_backend_name=generic-backend-2
 ...

And the configuration group name will be passed in so that all calls to
configuration.volume_group within that instance will be mapped to the proper
named group.

This class also ensures the implementation's configuration is grafted into the
option group. This is due to the way cfg works. All cfg options must be defined
and registered in the group in which they are used.
"""

from oslo_config import cfg

CONF = cfg.CONF


class Configuration(object):

    def __init__(self, opts, config_group=None):
        """Graft config values into config group.

        This takes care of grafting the implementation's config values
        into the config group.
        """
        self.config_group = config_group

        # set the local conf so that __call__'s know what to use
        if self.config_group:
            self._ensure_config_values(opts)
            self.local_conf = CONF._get(self.config_group)
        else:
            self.local_conf = CONF

    def _ensure_config_values(self, opts):
        CONF.register_opts(opts,
                           group=self.config_group)

    def append_config_values(self, opts):
        self._ensure_config_values(opts)

    def safe_get(self, value):
        try:
            return self.__getattr__(value)
        except cfg.NoSuchOptError:
            return None

    def __getattr__(self, value):
        """Retrieve the configuration value."""
        return getattr(self.local_conf, value)
