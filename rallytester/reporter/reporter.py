import logging
import socket
import yaql

from oslo_config import cfg
from rallytester import conf
from rallytester.reporter import configuration as config
from rallytester.trimmer import MsgTrimmer
from stevedore.driver import DriverManager

log = logging.getLogger(__name__)
CONF = conf.CONF

backend_opts = [
    cfg.StrOpt(
        'driver',
        default='influx',
        help='Driver to use for backend'),
]


def get_env(context):
    return context['__env']


def trimmer(context, text):
    return context['__trimmer'].trim(text)


class Reporter(object):
    """Class to send report status to influxdb."""

    def __init__(self):
        self.ctx = yaql.create_context()
        self.ctx.register_function(get_env, name='env')
        self.ctx.register_function(trimmer, name='trimmer')
        self.engine = yaql.factory.YaqlFactory().create()
        self.backends = []
        for backend in CONF.reporter.enabled_backends:
            configuration = config.Configuration(
                backend_opts, config_group=backend)
            self.backends.append(
                DriverManager(
                    namespace='rallytester.reporter.backends',
                    name=configuration.driver,
                    invoke_on_load=True,
                    invoke_kwds={'configuration': configuration},
                ).driver
            )

    def send_report(self, task, task_args, report):
        scenario = task.split('/')[-1].replace('.json', '')
        component = task.split('/')[-2].replace('.json', '')

        # Remove architecture
        for arch in ['_x86_64', '_aarch64']:
            scenario = scenario.replace(arch, '')
            component = component.replace(arch, '')

        measurements = self.analyze_report(
            component=component,
            scenario=scenario,
            extra_tags=task_args,
            report=report)
        self.report_measurements(measurements)

    def analyze_report(self, component, scenario, extra_tags, report):
        context = self.ctx.create_child_context()
        context['__env'] = {
            'component': component,
            'scenario': scenario,
            'host': socket.gethostname(),
            'extra_tags': {"arg_" + k: v for k, v in extra_tags.items()}
        }
        context['__trimmer'] = MsgTrimmer()
        return self.engine(CONF.reporter.yaql_expression).evaluate(
            data=report,
            context=context
        )

    def report_measurements(self, measurements):
        for backend in self.backends:
            backend.report_measurements(measurements)
