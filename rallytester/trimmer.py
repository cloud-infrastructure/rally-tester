import logging
import re

from functools import reduce
from rallytester import conf

log = logging.getLogger(__name__)
CONF = conf.CONF


class MsgTrimmer(object):
    """Class to trim error messages stored in rally db."""

    def __init__(self):
        self.list_regex_fun = []
        self.loadRegexps()
        self.msg_trim = self.nestFunctions()

    def loadRegexps(self):
        """Read the configuration items and set regexps."""
        for item in CONF.regex.enabled_regex:
            if (item in CONF.regex.regex_dict
                    and item in CONF.regex.substitution_dict):
                self.setRegexFunctions(
                    fun_name=item,
                    regexp=CONF.regex.regex_dict[item],
                    replacement=CONF.regex.substitution_dict[item]
                )

        def shorten_string(x, maxchar=CONF.regex.max_string_chars):
            return x[:maxchar]
        self.list_regex_fun.append(shorten_string)

    def setRegexFunctions(self, fun_name, regexp, replacement):
        """Create the re.sub function based on input params."""
        def afun(x):
            return re.sub(regexp, replacement, x)
        afun.__name__ = fun_name
        self.list_regex_fun.append(afun)

    def nestFunctions(self):
        """To nest the regex functions."""
        log.debug("the Trim function is f(x) = %s",
                  reduce(lambda x, y: "%s(%s)" % (y, x),
                         [i.__name__ for i in self.list_regex_fun], 'x'))

        return lambda msg: reduce(lambda x, y: y(x), self.list_regex_fun, msg)

    def trim(self, msg):
        return self.msg_trim(msg)
