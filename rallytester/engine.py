import json
import logging
import os
import re
import subprocess  # nosec
import yaml

from rallytester.reporter.reporter import Reporter
from rallytester.trimmer import MsgTrimmer

log = logging.getLogger(__name__)

# Regular expressions
TASK_ID_PATTERN = r'^Using task: ([a-z0-9-]{36})'
REASONS_PATTERN = r'^Reason\(s\):\n([\s\S]*)'


class RallyEngine(object):
    def __init__(self):
        self.trimmer = MsgTrimmer()
        self.reporter = Reporter()

    def msg_enrich(self, msg):
        """Enrich messages.

        In order to build categories of error messages,
        printout the error message contained in msg,
        and enrich it with the same message trimmed of all identifiers
        the structure is
        "{0} <sep-rally-sep>{1}".format(msg, trimmed_msg)
        """
        trimmed_msg = ""
        if self.trimmer:
            trimmed_msg = self.trimmer.trim(msg)
            return "{0} <sep-rally-sep> {1}".format(msg, trimmed_msg)
        return msg

    def get_pattern(self, text, pattern):
        """Extract a patter from text passed as parameter.

        :args text: The text to perform the search
        :args pattern: regular expresion to search for
        :returns: Empty string if the regex cannot be found, the value
            otherwise
        """
        match = re.search(pattern, text, re.MULTILINE)

        if match:
            return match.group(1)
        else:
            return ''

    def execute_task(self, deployment, task, task_args):
        """Execute the rally task and get the results.

        Example:
        # In success:
        >> result, error = execute_task(deployment, task)
        >> print result
        {'duration': 31.2, 'task': '/path/to/task',
        'scenario': 'Nova.list_servers', 'succeed': 1...}
        >> print error
        None

        # In error:
        >> result, error = execute_task(deployment, task)
        >> print result
        {'task': ..., 'succeed': 0}

        :returns: A tuple which first argument is a dict with
            useful information to processed by morphlines and a second argument
            that can be None, if the rally test went fine or an error message,
            if something didn't go well in the rally test
        """
        if not os.path.exists(task):
            msg = 'Task %s not found. Check you have put the correct path.' % (
                task)
            log.error('Task failed: %s', self.msg_enrich(msg))
            return {'task': task, 'succeed': 0}, msg

        log.debug("Starting rally task")

        with open(task, 'r') as taskfile:
            task_descriptor = taskfile.read()

        # Execute manually rally
        process = subprocess.Popen([  # nosec
            '/usr/bin/rally', 'task', 'start',
            '--deployment', deployment,
            '--task', task,
            '--task-args', yaml.dump(task_args)
        ], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        boutput, stderr = process.communicate()
        output = boutput.decode('utf-8')

        # Get the task ID from the output
        task_id = self.get_pattern(output, TASK_ID_PATTERN)
        log.info("Task id: {0}".format(task_id))

        # Check for existence of reason inside the output,
        # it indicates errors during the execution
        reasons_str = self.get_pattern(output, REASONS_PATTERN)

        if reasons_str:
            reasons = reasons_str.splitlines()
            msg = "{0} (1 of {1})".format(
                reasons[0],
                len(reasons)
            )
            log.error('Task failed: %s', self.msg_enrich(msg))
            return (
                {
                    'task': task,
                    'succeed': 0
                },
                "{0}\nComplete output of `rally task start`: \n{1}\n"
                "STDERR: \n{2}\nDeployment\n{3}\nTask descriptor:\n{4}"
                .format(msg, output, stderr, deployment, task_descriptor)
            )

        if not task_id:
            # This means that something went wrong when executing the task
            msg = 'Error executing rally task. ' \
                  'Check the deployment is correctly configured.'
            log.error('Task failed: %s', self.msg_enrich(msg))
            return (
                {
                    'task': task,
                    'succeed': 0
                },
                "{0}\nComplete output of `rally task start`: \n{1}\n"
                "STDERR: \n{2}\nDeployment\n{3}\nTask descriptor:\n{4}"
                .format(msg, output, stderr, deployment, task_descriptor)
            )

        # Get results executing rally command
        process = subprocess.Popen([  # nosec
            '/usr/bin/rally', 'task', 'report', task_id, '--json'],
            stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        boutput, stderr = process.communicate()
        output = boutput.decode('utf-8')

        if process.returncode:
            # This means that something is wrong when you execute the task
            msg = ('Error retrieving rally task results.'
                   ' Check the deployment is correctly configured.')
            log.error('Task failed: %s', self.msg_enrich(msg))
            return (
                {
                    'task': task,
                    'succeed': 0
                },
                "{0}\nComplete output of `rally task results`:\n{1}\n \n{2}\n"
                "STDERR: \n{3}\nTask descriptor:\n{4}"
                .format(msg, task_id, output, stderr, task_descriptor)
            )

        # Get the JSON result
        try:
            json_result = json.loads(output)
        except ValueError:
            msg = 'Rally report is not a JSON. Cannot process results.'
            log.error('Task failed: %s', self.msg_enrich(msg))
            return ({
                'task': task,
                'succeed': 0},
                "{0}\nComplete output of `rally task result {3}`: \n{1}\n"
                "STDERR: \n{2}\nTask descriptor:\n{4}"
                .format(msg, output, stderr, task_id, task_descriptor)
            )

        # Send the report to influxDB
        Reporter().send_report(
            task=task,
            task_args=task_args,
            report=json_result,
        )

        # Check if the task is crashed
        if json_result['tasks'][0]["status"] == "crashed":
            msg = 'Rally task has crashed.'
            log.error('Task failed: %s', self.msg_enrich(msg))
            return ({
                'task': task,
                'succeed': 0},
                "Rally task {0} has crashed\n\nTask descriptor:\n{1}"
                .format(task_id, task_descriptor)
            )

        scenarios = ",".join(
            s['title'] for s in json_result['tasks'][0]['subtasks'])

        # Magnum
        if any(sla['criterion'] == 'something_went_wrong' for sla in
                json_result['tasks'][0]['subtasks'][0]['workloads'][0]
                ['sla_results']['sla']):

            msg = ""
            tracebacks = ""
            for sla in (json_result['tasks'][0]['subtasks'][0]
                        ['workloads'][0]['sla_results']['sla']):
                msg += ('\n' + sla['detail'])
                log.error('Task failed: %s',
                          self.msg_enrich(msg.replace('\n', ' ')))
            return (
                {
                    'task': task,
                    'scenarios': scenarios,
                    'succeed': 0
                },
                "{0}\n{1}\nTask descriptor:\n{2}".format(msg, tracebacks,
                                                         task_descriptor)
            )

        # If there is any error in the report
        if any(data['error'] for data in
                json_result['tasks'][0]['subtasks'][0]['workloads']
                [0]['data']):

            msg = ""
            tracebacks = ""
            for data in (json_result['tasks'][0]['subtasks'][0]
                         ['workloads'][0]['data']):
                # The real error is in the first result
                # and the second element of the array
                msg += ('\n' + data['error'][1])
                tracebacks += ('\n' + data['error'][2])
                log.error('Task failed: %s',
                          self.msg_enrich(msg.replace('\n', ' ')))
            return (
                {
                    'task': task,
                    'scenarios': scenarios,
                    'succeed': 0
                },
                "{0}\n{1}\nTask descriptor:\n{2}".format(msg, tracebacks,
                                                         task_descriptor)
            )

        # Json error2
        if any(sla['success'] is False for sla in
                json_result['tasks'][0]['subtasks'][0]['workloads']
                [0]['sla_results']['sla']):

            msg = ""
            tracebacks = ""
            return (
                {
                    'task': task,
                    'scenarios': scenarios,
                    'succeed': 0
                },
                "{0}\n{1}\nTask descriptor:\n{2}".format(msg, tracebacks,
                                                         task_descriptor)
            )

        # In any other case
        log.info('So far, so good, the task has been sucessfully completed')
        return (
            {
                'task': task,
                'scenarios': scenarios,
                'succeed': 1,
                'duration': (
                    json_result['tasks'][0]['subtasks'][0]
                    ['workloads'][0]['full_duration'])
            },
            None  # No errors
        )
