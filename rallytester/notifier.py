import logging
import smtplib
import socket

from email.mime.text import MIMEText  # py3
from email import utils  # py3

log = logging.getLogger(__name__)


def print_result(result):
    """Print result in a easy format to be parsed by morphlines afterwards.

    Example:
    >> my_dict = {'a': 'b', 'foo': 'ba'}
    >> print_result(my_dict)
    2017-02-16 16:18:37.787 25410 INFO rallytester.notifier [-]
    Results: a=b foo=ba

    :args result: The result of the rally test
    """
    log.info("Results: " + " ".join(["{0}={1}".format(key, value)
                                     for key, value in result.items()]))


def send_alarm(task, deployment, error_msg, alert_to):
    """Send an email notifying of an error in the rally test.

    :args task: The task we are testing
    :args deployment: The deployment where we are executing the task
    :args error_msg: The error message we are going to send via email
    :args alert_to: Destination for our email
    """
    if alert_to:
        log.debug("Sending alert to %s" % alert_to)

        # Prepare the email
        msg = MIMEText(
            """Failure in deployment {0} for task {1}

Error message: {2}""".format(deployment, task, error_msg))

        msg['Subject'] = ("[Failure] Rally Test ({0}) "
                          "in task {1}").format(deployment, task)
        msg['From'] = '%s-noreply@cern.ch' % socket.getfqdn()
        msg['To'] = alert_to
        msg['Date'] = utils.formatdate()

        # Send email
        s = smtplib.SMTP('localhost')
        s.sendmail(
            msg['From'],
            msg['To'].split(','),
            msg.as_string())
        s.quit()
