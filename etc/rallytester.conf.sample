[DEFAULT]


[alert]

#
# From rallytester
#

# Notify via mail in case of errors (string value)
#alert_to = <None>


[influx]

#
# From rallytester
#

# Host to use for influxdb (string value)
#host = dbod-ccinflux.cern.ch

# Port to use for influxdb (integer value)
#port = 8087

# Database for influxdb (string value)
#database = dblogger

# Defalt user for influxdb (string value)
#user = fake_user

# Defalt password for influxdb (string value)
#password = fake_password

# Use ssl to connect to the database (boolean value)
#ssl = true

# List of measurements allowed to be stored in the backend (list value)
#allowlist =

# List of measurements not allowed to be stored in the backend (list value)
#denylist =


[logging]

#
# From rallytester
#

# Log level. Defaults to INFO (string value)
#log_level = INFO

# Log file where to store execution logs (string value)
#log_file = /var/log/rally-tester/rally-tester.log


[regex]

#
# From rallytester
#

# Ordered list of enabled regular expressions to be used while trimming the
# error message. (list value)
#enabled_regex = raw_substring,cluster,httpheaderdict,json_message,http_replace,ip_replace,id_rally_replace,req_replace,long_id_replace,resource_replace,quota_exceeded,rally_tired,remote_error,resource_limit,invalid_flavor,traceback

# Regular expressions available for trimming. (dict value)
#regex_dict = cluster:([^<]*<\S*)\s*\{[^>]*(>.*),http_replace:(http[s]?\:\/\/)([^\/]*)/\S*,httpheaderdict:(.*)HTTPHeaderDict\(\{.*,id_rally_replace:rally\-([\w\-]{8,}),invalid_flavor:^(\S* plugin) (\S*) doesn't pass (\S*) validation. Details: .*,ip_replace:[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3},json_message:Resource (.*)\'message\': u(\'|")(.*)(\'|"), u\'code\'(.*),long_id_replace:([a-f0-9]{32}|[a-f0-9\-]{36}),quota_exceeded:Requested [0-9]*, [0-9]*, but already used [0-9]*, [0-9]* of [0-9]*, [0-9]*,rally_tired:(Rally tired waiting)\s([0-9]*\.00 seconds for|for)\s(\S*)\s\S* to become (\([^\)]*\)|([^\(]\S*)) .*,raw_substring:.* (ERROR rallytester\.rallytester)\s\[[^\]]*\]\s\[[^\]]*\]\s(Task failed:\s*|)(.*),remote_error:(Remote error: )(\S*\s).*,req_replace:req-[\w\-\:]{12,},resource_limit:(.*)(Resource limit exceeded: .* per project)(.*),resource_replace:Resource [a-z0-9\-]{15,},traceback:(\n)?Traceback \(most recent call last\)[\s\S]*

# Substitutions that can be executed if there is a match on regular
# expressions. (dict value)
#substitution_dict = cluster:\1\2,http_replace:\1\2/...,httpheaderdict:\1HTTPHeaderDict,id_rally_replace:rally-xyz,invalid_flavor:Task \2 fail \3,ip_replace:xyz.xyz.xyz.xyz,json_message:\3,long_id_replace:xyz,quota_exceeded:Requested .....,rally_tired:waiting for \3 to become \4,raw_substring:\3,remote_error:\1 \2,req_replace:req-xyz,resource_limit:\2,resource_replace:Resource xyz,traceback:

# Maximum length of the trimmed string (integer value)
#max_string_chars = 300


[reporter]

#
# From rallytester
#

# A list of backend names to use. These backend names should be backed by a
# unique [CONFIG] group with its options (list value)
#enabled_backends = influx

# YAQL expression to fetch information from report (string value)
#yaql_expression = "
#    let(
#      deployment => $$.tasks[0].env_name,
#      component => env().component,
#      scenario => env().scenario,
#      extra_tags => env().extra_tags,
#      time => $$.tasks[0].created_at,
#      error =>
#        selectCase(
#          ($$.tasks[0].subtasks and $$.tasks[0].subtasks[0].workloads[0].data),
#          ($$.tasks[0].subtasks and list($$.tasks[0].subtasks[0].workloads[0].contexts_results.where($$.setup.error))),
#          ($$.tasks[0].status = 'crashed' or (not $$.tasks[0].pass_sla and not $$.tasks[0].subtasks[0].workloads[0].data))
#        ).switchCase(
#          trimmer($$.tasks[0].subtasks[0].workloads[0].data[0].error.defaultIfEmpty([""]).aggregate($1 + "
#    " +$2)),
#          trimmer(list($$.tasks[0].subtasks[0].workloads[0].contexts_results.where($$.setup.error))[0].setup.error.aggregate($1 + "
#    " + $2)),
#          "crashed"
#      ),
#      data => selectCase($$.tasks[0].status = 'crashed' or list($.tasks[0].subtasks[0].workloads[0].contexts_results.where($$.setup.error)) or (not $$.tasks[0].pass_sla and not $$.tasks[0].subtasks[0].workloads[0].data))
#        .switchCase(null, $$.tasks[0].subtasks[0].workloads[0].statistics.durations['total'].data)
#    ) -> selectCase($$.tasks[0].status = 'crashed' or list($$.tasks[0].subtasks[0].workloads[0].contexts_results.where($$.setup.error)) or (not $$.tasks[0].pass_sla and not $$.tasks[0].subtasks[0].workloads[0].data)).switchCase(
#      list(
#        dict(
#          measurement => 'rally_task',
#          tags => dict(
#            deployment => $$deployment,
#            component => $$component,
#            scenario => $$scenario,
#            task => "total",
#            error => $$error
#          ) + $$extra_tags,
#          time => $$time,
#          fields => dict(
#            duration => 0.0,
#            failure => 1
#          )
#        )
#      ), (
#        list(
#          dict(
#            measurement => 'rally_task',
#            tags => dict(
#              deployment => $$deployment,
#              component => $$component,
#              scenario => $$scenario,
#              task => "total",
#              error => $$error
#            ) + $$extra_tags,
#            time => $$time,
#            fields => dict(
#                duration => $$data.max,
#                failure => switch(
#                    $$data.success = "100.0%" => 0,
#                    $$data.success != "100.0%" => 1
#                )
#            )
#          )
#        ) + $$.tasks[0].subtasks[0].workloads[0].statistics.durations.atomics.selectMany(
#          dict(
#            measurement => 'rally_task',
#            tags => dict(
#              deployment => $$deployment,
#              component => $$component,
#              scenario => $$scenario,
#              task => $$.name,
#              error => $$error
#            ) + $$extra_tags,
#            time => $$time,
#            fields => dict(
#              duration => $$.data.max,
#              failure => switch(
#                $$.data.success = "100.0%" => 0,
#                $$.data.success != "100.0%" => 1
#              )
#            )
#          )
#        ) + selectCase(len($$.tasks[0].subtasks[0].workloads[0].statistics.durations.atomics.children.where($$)) = 0).switchCase(
#          [],
#          list($$.tasks[0].subtasks[0].workloads[0].statistics.durations.atomics.children.where($$))[0].selectMany(
#            dict(
#              measurement => 'rally_task',
#              tags => dict(
#                deployment => $$deployment,
#                component => $$component,
#                scenario => $$scenario,
#                task => $$.name,
#                error => $$error
#              ) + $$extra_tags,
#              time => $$time,
#              fields => dict(
#                duration => $$.data.max,
#                failure => switch(
#                  $$.data.success = "100.0%" => 0,
#                  $$.data.success != "100.0%" => 1
#                )
#              )
#            )
#          )
#        )
#      )
#    )"
